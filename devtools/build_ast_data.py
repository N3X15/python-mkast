from __future__ import annotations

import argparse
import ast
import os
import shutil
import subprocess
import sys
import tempfile
from enum import IntFlag
from pathlib import Path
from typing import Any, Dict, List, Optional, Set, Tuple, Type

import black
import isort

# from mkast.formatters.autoflake import AutoflakeFormattingProvider
# from mkast.formatters.black import BlackFormattingProvider
# from mkast.formatters.isort import ISortFormattingProvider
# from mkast.generators.astor import AstorCodeGenerator

from ruamel.yaml import YAML as Yaml  # isort: skip

YAML = Yaml(typ="rt", pure=True)
FORMAT_VERSION: int = 0

PATH_TYPESHED = Path("lib") / "typeshed"

TYPE_TRANSLATE = {
    "_Slice": "expr",
}

SUPPORTED_PYVERSIONS: List[Tuple[int, int]] = [
    # (3, 6),
    # (3, 7),
    # (3, 8),
    # (3, 9),
    (3, 10),
    (3, 11),
    (3, 12),
]


def get_version() -> Dict[str, Any]:
    TYPESHED_COMMIT: str
    try:
        cd = Path.cwd()
        o: bytes = subprocess.check_output(
            ["git", "rev-parse", "HEAD"], cwd=cd / "lib" / "typeshed"
        )
        TYPESHED_COMMIT = o.decode("utf-8").strip()
    except:
        TYPESHED_COMMIT = "N/A"
    return {
        "python": {
            "major": sys.version_info.major,
            "minor": sys.version_info.minor,
            "micro": sys.version_info.micro,
            "releaselevel": sys.version_info.releaselevel,
            "serial": sys.version_info.serial,
        },
        "typeshed": {
            "commit": TYPESHED_COMMIT,
        },
        "fmt": FORMAT_VERSION,
    }


# class MKAstCodeGenerator(AstorCodeGenerator):
#     def __init__(self) -> None:
#         super().__init__()
#         self.addFormatter(
#             ISortFormattingProvider(isort_config=isort.Config(line_length=65355))
#         )
#         self.addFormatter(AutoflakeFormattingProvider(remove_all_unused_imports=True))
#         self.addFormatter(BlackFormattingProvider(black.Mode()))


def write_yaml_to(data: Any, path: Path) -> None:
    print(f"YAML\t{path}")
    tmpfile = ""
    try:
        with tempfile.NamedTemporaryFile(
            "w", delete=False, dir=path.parent, suffix="~"
        ) as f:
            YAML.dump_all(data, f)
            tmpfile = f.name
        os.replace(tmpfile, path)
    except:
        if os.path.isfile(tmpfile):
            os.unlink(tmpfile)
        raise


def generate_formatted_code(ast: ast.AST) -> str:
    import astor, black, autoflake

    code = astor.to_source(ast)
    code = autoflake.fix_code(
        code,
        additional_imports=None,
        expand_star_imports=False,
        remove_all_unused_imports=False,
        remove_duplicate_keys=False,
        remove_unused_variables=False,
        remove_rhs_for_unused_variables=False,
        ignore_init_module_imports=False,
        ignore_pass_statements=False,
        ignore_pass_after_docstring=False,
    )
    code = black.format_str(code, mode=black.FileMode())
    return isort.code(code, config=isort.Config())


def write_python_to(ast: ast.AST, path: Path) -> None:
    print(f"PY3\t{path}")
    tmpfile = ""
    try:
        with tempfile.NamedTemporaryFile(
            "w", delete=False, dir=path.parent, suffix="~"
        ) as f:
            f.write(generate_formatted_code(ast))
            tmpfile = f.name
        os.replace(tmpfile, path)
    except Exception as e:
        if os.path.isfile(tmpfile):
            os.unlink(tmpfile)
        raise


def deserialize_type(s: str) -> type:
    """
    Module(body=[Expr(value=Name(id='expr', ctx=Load()))], type_ignores=[])
    """
    mod = ast.parse(s)
    if isinstance(mod, ast.Module):
        if isinstance(mod.body[0], ast.Expr):
            return mod.body[0].value
    sys.exit(ast.dump(mod))


def serialize_type(t: ast.AST) -> str:
    return ast.unparse(t)


class EBuildCallType(IntFlag):
    NONE = 0
    CALL = 1
    IF_AST = 2
    NOT_NULL = 3


class NodeAttribute:
    def __init__(self) -> None:
        self.id: str = ""
        self.type: ast.AST = None
        self.optional: bool = False
        self.inherited: bool = False
        self.in_match_values: bool = False

    def inherit(self) -> NodeAttribute:
        n = NodeAttribute()
        n.id = self.id
        n.type = self.type
        n.optional = self.optional
        n.in_match_values = self.in_match_values
        n.inherited = True
        return n

    @classmethod
    def FromAST(cls: Type[NodeAttribute], ass: ast.AnnAssign) -> NodeAttribute:
        na = cls()
        na.id = ass.target.id
        na.type = ass.annotation

        if isinstance(na.type, ast.BinOp):
            binop: ast.BinOp = na.type
            if isinstance(binop.right, ast.Constant) and binop.right.value is None:
                na.type = binop.left
                na.optional = True
        return na

    def _post_parse(self, node: Node) -> "NodeAttribute":
        self.in_match_values = self.id in node.match_args
        return self

    def is_simple_type(self) -> bool:
        if isinstance(self.type, ast.Name):
            match self.type.id:
                case "bool" | "float" | "int" | "str":
                    return True
                case "list" | "dict":
                    return True
        elif isinstance(self.type, ast.Subscript):
            if isinstance(self.type.slice, ast.Name):
                match self.type.slice.id:
                    case "bool" | "float" | "int" | "str":
                        return True
                    # case "list" | "dict":
                    #     return True
        return False

    def _build_call_type(self) -> EBuildCallType:
        if self.is_simple_type():
            return EBuildCallType.NONE
        if isinstance(self.type, ast.Attribute):
            if isinstance(self.type.value, ast.Name):
                if self.type.value.id == "typing":
                    match self.type.attr:
                        case "Any" | "Union":
                            # HACK - Fix ast.Constant
                            if self.id in ("value", "s"):
                                return EBuildCallType.NONE
                            return (
                                EBuildCallType.CALL
                            )  # EBuildCallType.IF_AST# | EBuildCallType.NOT_NULL
        if self.optional:
            return EBuildCallType.NOT_NULL
        return EBuildCallType.CALL

    def is_any_type(self) -> bool:
        if isinstance(self.type, ast.Attribute):
            if isinstance(self.type.value, ast.Name):
                return self.type.value.id == "typing" and self.type.attr == "Any"
        return False

    def call_build(self, from_: ast.AST) -> ast.AST:
        ct = self._build_call_type()
        if ct == EBuildCallType.NONE:
            return from_
        call: ast.AST = ast.Call(
            func=ast.Attribute(
                value=from_,
                attr="build",
            ),
            args=[],
            keywords=[],
        )
        if isinstance(self.type, ast.Subscript):
            if isinstance(self.type.value, ast.Name):
                if self.type.value.id == "list":
                    # print(self.id, ast.dump(self.type),self.type.value.id)
                    call = ast.ListComp(
                        elt=ast.Call(
                            func=ast.Attribute(
                                value=ast.Name(id="x", ctx=ast.Load()),
                                attr="build",
                                ctx=ast.Load(),
                            ),
                            args=[],
                            keywords=[],
                        ),
                        generators=[
                            ast.comprehension(
                                target=ast.Name(id="x", ctx=ast.Store()),
                                iter=from_,
                                ifs=[],
                                is_async=0,
                            ),
                        ],
                    )
        if (ct & EBuildCallType.IF_AST) != 0:
            call = ast.IfExp(
                test=ast.Call(
                    func=ast.Name(id="isinstance", ctx=ast.Load()),
                    args=[
                        from_,
                        ast.Name(id="AST", ctx=ast.Load()),
                        # ast.Attribute(
                        #     value=ast.Name(id='ast'),
                        #     attr='AST'
                        # )
                    ],
                    keywords=[],
                ),
                body=call,
                orelse=from_,
            )
        if (ct & EBuildCallType.NOT_NULL) != 0:
            call = ast.IfExp(
                test=ast.Compare(
                    left=from_, ops=[ast.IsNot()], comparators=[ast.Constant(None)]
                ),
                body=call,
                orelse=ast.Constant(None),
            )
        # if (ct & (EBuildCallType.NOT_NULL|EBuildCallType.IF_AST)) > 0:
        #     tests: List[ast.AST] = []
        #     test: ast.AST
        #     if (ct & EBuildCallType.NOT_NULL)>0:
        #         tests.append(ast.Compare(
        #             left=from_, ops=[ast.IsNot()], comparators=[ast.Constant(None)]
        #         ))
        #     if (ct & EBuildCallType.IF_AST)>0:
        #         tests.append(ast.Call(func=ast.Name(id='isinstance', ctx=ast.Load()), args=[
        #             from_,
        #             ast.Name(id='AST', ctx=ast.Load()),
        #         ], keywords=[]))
        #     if len(tests)>1:
        #         test = ast.BoolOp(
        #             op=ast.And(),
        #             values=tests
        #         )
        #     else:
        #         test = tests[0]
        #     call = ast.IfExp(
        #         test=test,
        #         body=call,
        #         orelse=ast.Constant(None),
        #     )
        return call

    def get_default(self) -> ast.AST:
        if self.optional or self.is_any_type():
            return ast.Constant(None)
        if isinstance(self.type, ast.Name):
            match self.type.id:
                case "bool":
                    return ast.Constant(False)
                case "float":
                    return ast.Constant(0.0)
                case "int":
                    return ast.Constant(0)
                case "str":
                    return ast.Constant("")
                case "list":
                    return ast.List(elts=[])
                case "dict":
                    return ast.Dict(keys=[], values=[])
        elif isinstance(self.type, ast.Attribute):
            # print(ast.dump(self.type)) # typing.Union
            return ast.Constant(None)
        elif isinstance(self.type, ast.Subscript):
            if isinstance(self.type.value, ast.Name):
                match self.type.value.id:
                    case "List":
                        return ast.List(elts=[])
                    case "Dict":
                        return ast.Dict(keys=[], values=[])
        else:
            sys.exit(type(self.type))
        return ast.Constant(None)

    def get_anno_type(self) -> ast.AST:
        anno: ast.AST = self.type
        if self.optional:
            anno = ast.Subscript(
                value=ast.Attribute(
                    value=ast.Name("typing", ctx=ast.Load()),
                    attr="Optional",
                    ctx=ast.Load(),
                ),
                slice=anno,
                ctx=ast.Load(),
            )
        return anno

    def to_annassign(
        self, value: Optional[ast.AST] = None, selfish: bool = False
    ) -> ast.AnnAssign:
        t: ast.expr = ast.Name(self.id, ctx=ast.Store())
        if selfish:
            t = ast.Attribute(value=ast.Name("self", ctx=ast.Store()), attr=self.id)
        o = ast.AnnAssign(
            target=t,
            annotation=self.type,
            value=value or self.get_default(),
            simple=1,
        )
        if self.optional:
            o.annotation = ast.Subscript(
                value=ast.Attribute(
                    value=ast.Name("typing", ctx=ast.Load()),
                    attr="Optional",
                    ctx=ast.Load(),
                ),
                slice=o.annotation,
                ctx=ast.Load(),
            )
        return o

    def serialize(self) -> dict:
        return {
            "id": self.id,
            "type": ast.unparse(self.type),
            "optional": self.optional,
            "inherited": self.inherited,
            "in_match_values": self.in_match_values,
        }

    def deserialize(self, data: dict) -> None:
        self.id = data["id"]
        self.type = deserialize_type(data["type"])
        self.optional = data["optional"]
        self.inherited = data["inherited"]
        self.in_match_values = data["in_match_values"]

    def _postprocess(self, tree: Tree, node: Node) -> None:
        if isinstance(self.type, ast.BinOp):
            o = []
            c: ast.BinOp = self.type
            while True:
                if not isinstance(c.left, ast.BinOp):
                    o.append(c.left)
                    o.append(c.right)
                    break
                if isinstance(c.right, ast.Name):
                    o.append(c.right)
                c = c.left
            self.type = ast.Subscript(
                value=ast.Attribute(
                    value=ast.Name("typing", ctx=ast.Load()), attr="Union"
                ),
                slice=ast.Tuple(elts=o),
            )
        self.in_match_values = self.id in node.match_args


class Node:
    ANCESTOR_DIR_CACHE: Dict[type, Set[str]] = {}

    def __init__(self) -> None:
        self.id: str = ""
        self.bases: List[str] = []
        self.match_args: List[str] = []
        self.attributes: Dict[str, NodeAttribute] = {}

        self._pp_done: bool = False

    def _inherit_from(self, tree: Tree, parent: Node) -> None:
        parents: Set[Node] = set()
        parent._postprocess(tree)
        parents.add(parent)
        for basename in parent.bases:
            pn = tree.nodes[basename]
            pn._postprocess(tree)
            parents.add(pn)
        for pn in parents:
            for na in pn.attributes.values():
                if na.id.startswith("_"):
                    continue
                if na.id in self.attributes.keys():
                    continue
                print(f"{self.id}: Inheriting {na.id} from {pn.id}")
                self.attributes[na.id] = na.inherit()

    def parse_from(self, node: ast.ClassDef) -> None:
        self.id = node.name
        self.bases = [x.id for x in node.bases if isinstance(x, ast.Name)]
        bn: ast.AST
        for bn in allnodesin(node):
            match type(bn):
                case ast.Assign:
                    # print(ast.dump(bn.value))
                    if bn.targets[0].id == "__match_args__":
                        self.match_args = [x.value for x in bn.value.elts]
                case ast.AnnAssign:
                    na = NodeAttribute.FromAST(bn)
                    # if na.id.startswith("_"):
                    #     continue
                    self.attributes[na.id] = na
        self.attributes = {
            k: v._post_parse(self)
            for k, v in self.attributes.items()
            if not k.startswith("_")
        }

    def to_frontend_classdef(
        self,
        *,
        major: int = 0,
        minor: int = 0,
        mixins: List[str] = [],
        backend_module: str = "",
        mixins_module: str = "",
    ) -> ast.ClassDef:
        bases: List[ast.AST] = [
            ast.Attribute(value=ast.Name(mixins_module), attr=x) for x in mixins
        ]
        bases.append(ast.Attribute(value=ast.Name(backend_module), attr=self.id))
        cd = ast.ClassDef(
            name=self.id,
            bases=bases,
            keywords=[],
            body=[],
            decorator_list=[],
        )
        init = ast.FunctionDef(
            name="__init__",
            args=ast.arguments(
                posonlyargs=[],
                args=[ast.Name("self")],
                vararg=None,
                kwonlyargs=[],
                kw_defaults=[],
                kwarg=None,
                defaults=[],
            ),
            body=[],
            decorator_list=[],
            returns=ast.Constant(None),
        )
        cd.body.append(init)
        needs_super = True
        kwafter = []
        kwdefafter = []
        superkw = []
        for na in self.attributes.values():
            if na.id.startswith("_"):
                continue
            kwafter.append(ast.arg(arg=na.id, annotation=na.get_anno_type()))
            kwdefafter.append(na.get_default())
            superkw.append(ast.keyword(arg=na.id, value=ast.Name(na.id)))
        init.body.append(
            ast.Expr(
                value=ast.Call(
                    func=ast.Attribute(
                        value=ast.Call(
                            func=ast.Name(id="super", ctx=ast.Load()),
                            args=[],
                            keywords=[],
                        ),
                        attr="__init__",
                        ctx=ast.Load(),
                    ),
                    args=[],
                    keywords=superkw,
                )
            ),
        )
        init.args.kwonlyargs += kwafter
        init.args.kw_defaults += kwdefafter

        if len(init.body) == 0:
            init.body.append(ast.Pass())
        if len(cd.body) == 0:
            cd.body.append(ast.Pass())
        return cd

    def to_classdef(self) -> ast.ClassDef:
        if len(self.bases) == 0 and self.id != "AST":
            print(f"W: Node.to_classdef(): Class {self.id} has 0 bases.")
        cd = ast.ClassDef(
            name=self.id,
            bases=[ast.Name(x) for x in self.bases],
            keywords=[],
            body=[],
            decorator_list=[],
        )
        init = ast.FunctionDef(
            name="__init__",
            args=ast.arguments(
                posonlyargs=[],
                args=[ast.Name("self")],
                vararg=None,
                kwonlyargs=[],
                kw_defaults=[],
                kwarg=None,
                defaults=[],
            ),
            body=[],
            decorator_list=[],
            returns=ast.Constant(None),
        )
        cd.body.append(init)

        init_ast_obj = ast.AnnAssign(
            target=ast.Name("n"),
            annotation=ast.Attribute(value=ast.Name("ast"), attr=self.id),
            value=ast.Call(
                func=ast.Attribute(value=ast.Name("ast"), attr=self.id),
                args=[],
                keywords=[],
            ),
            simple=1,
        )
        build = ast.FunctionDef(
            name="build",
            args=ast.arguments(
                posonlyargs=[],
                args=[ast.Name("self")],
                vararg=None,
                kwonlyargs=[],
                kw_defaults=[],
                kwarg=None,
                defaults=[],
            ),
            body=[init_ast_obj],
            decorator_list=[],
            returns=ast.Attribute(value=ast.Name("ast"), attr=self.id),
        )
        cd.body.append(build)

        needs_super = True
        kwafter = []
        kwdefafter = []
        superkw = []
        for na in self.attributes.values():
            if na.id.startswith("_"):
                continue
            # Nope
            # rtcall.keywords.append(
            #     ast.keyword(
            #         arg=na.id,
            #         value=na.call_build(
            #             ast.Attribute(value=ast.Name("self"), attr=na.id)
            #         ),
            #     )
            # )
            build.body.append(
                ast.Assign(
                    targets=[ast.Attribute(value=ast.Name("n"), attr=na.id)],
                    value=na.call_build(
                        ast.Attribute(value=ast.Name("self"), attr=na.id)
                    ),
                )
            )
            if na.inherited:
                kwafter.append(ast.arg(na.id, na.get_anno_type()))
                kwdefafter.append(na.get_default())
                superkw.append(ast.keyword(arg=na.id, value=ast.Name(na.id)))
                continue
            if needs_super:
                needs_super = False
                init.body.insert(
                    0,
                    ast.Expr(
                        value=ast.Call(
                            func=ast.Attribute(
                                value=ast.Call(
                                    func=ast.Name(id="super", ctx=ast.Load()),
                                    args=[],
                                    keywords=[],
                                ),
                                attr="__init__",
                                ctx=ast.Load(),
                            ),
                            args=[],
                            keywords=superkw,
                        )
                    ),
                )
            init.args.kwonlyargs.append(ast.arg(na.id, na.get_anno_type()))
            init.args.kw_defaults.append(na.get_default())
            init.body.append(
                na.to_annassign(value=ast.Name(na.id, ctx=ast.Load()), selfish=True)
            )
        if needs_super:
            needs_super = False
            init.body.insert(
                0,
                ast.Expr(
                    value=ast.Call(
                        func=ast.Attribute(
                            value=ast.Call(
                                func=ast.Name(id="super", ctx=ast.Load()),
                                args=[],
                                keywords=[],
                            ),
                            attr="__init__",
                            ctx=ast.Load(),
                        ),
                        args=[],
                        keywords=superkw,
                    )
                ),
            )
        init.args.kwonlyargs += kwafter
        init.args.kw_defaults += kwdefafter
        build.body.append(ast.Return(ast.Name("n")))

        if len(init.body) == 0:
            init.body.append(ast.Pass())
        if len(cd.body) == 0:
            cd.body.append(ast.Pass())
        return cd

    def serialize(self) -> dict:
        return {
            "name": self.id,
            "bases": self.bases,
            "match_args": self.match_args,
            "attributes": [x.serialize() for x in self.attributes.values()],
        }

    def deserialize(self, data: dict) -> str:
        self.id = data["name"]
        self.bases = data["bases"]
        self.match_args = data["match_args"]
        self.attributes = {}
        for d in data["attributes"]:
            a = NodeAttribute()
            a.deserialize(d)
            self.attributes[a.id] = a

    def _postprocess(self, tree: Tree) -> None:
        if self._pp_done:
            return
        self._pp_done = True
        for basename in self.bases:
            self._inherit_from(tree, tree.nodes[TYPE_TRANSLATE.get(basename, basename)])
        for na in self.attributes.values():
            na._postprocess(tree, self)


class FixBadNames(ast.NodeTransformer):
    def visit_Name(self, node: ast.Name) -> Any:
        match node.id:
            case "_Identifier":
                node.id = "str"
            case "_Slice":
                node.id = "expr"
            case "Any":
                node.id = "typing.Any"
            case "_Pattern":
                node.id = "typing.Pattern"
            case "Literal":
                node.id = "typing.Literal"
        return node


class ApplyConditionals(ast.NodeTransformer):
    def __init__(self, for_py_version: Tuple[int, int]) -> None:
        super().__init__()
        self.for_py_version: Tuple[int, int] = for_py_version

    def visit_If(self, node: ast.If) -> Any:
        (major, minor) = [x.value for x in node.test.comparators[0].elts]
        passed: bool = False
        l = tuple(map(int, (self.for_py_version[:2])))
        r = (int(major), int(minor))
        match node.test.ops[0]:
            case ast.Gt():
                passed = l > r
            case ast.GtE():
                passed = l >= r
            case ast.Lt():
                passed = l < r
            case ast.LtE():
                passed = l <= r
            case ast.Eq():
                passed = l == r
            case ast.NotEq():
                passed = l != r
            case _:
                raise Exception(node.test.ops[0])
        if passed:
            return node.body
        else:
            return node.orelse


class AllNodesVisitor(ast.NodeVisitor):
    def __init__(self) -> None:
        super().__init__()
        self.all_nodes: List[ast.AST] = []

    def visit(self, node: ast.AST) -> Any:
        self.all_nodes.append(node)
        return super().visit(node)


def allnodesin(n: ast.AST) -> List[ast.AST]:
    anv = AllNodesVisitor()
    anv.visit(n)
    return anv.all_nodes


class Tree:
    def __init__(self) -> None:
        self.nodes: Dict[str, Node] = {}
        self.order: List[str] = []
        self.mixins: Dict[str, List[str]] = {}

    @classmethod
    def ParseFrom(cls, filename: Path, for_py_version: Tuple[int, int]) -> Tree:
        t: Tree = cls()
        mod: ast.Module
        with filename.open("r") as f:
            mod = ast.parse(f.read(), filename=filename)
        mod = FixBadNames().visit(mod)
        mod = ApplyConditionals(for_py_version).visit(mod)
        for n in allnodesin(mod):
            if isinstance(n, ast.ClassDef):
                node = Node()
                node.parse_from(n)
                print("Found " + node.id)
                t.nodes[node.id] = node
        for n in t.nodes.values():
            n._postprocess(t)
        return t

    @classmethod
    def LoadYAMLFrom(cls, yamldir: Path) -> Tree:
        files: Set[Path] = set()
        order: List[str] = []
        with open(yamldir / "__index.yml", "r") as f:
            doc = list(YAML.load_all(f))
            assert doc[0]["fmt"] == FORMAT_VERSION
            files = set(map(Path, doc[1]["files"]))
            order = list(map(str, doc[1]["order"]))
        tree = cls()
        for filename in files:
            with open(filename, "r") as f:
                doc = list(YAML.load_all(f))
                assert doc[0]["fmt"] == FORMAT_VERSION
                n = Node()
                n.deserialize(doc[1])
                tree.nodes[n.id] = n
        tree.order = order
        return tree

    def write_module(self) -> ast.Module:
        mod = ast.Module(body=[], type_ignores=[])
        mod.body.append(
            ast.ImportFrom(
                module="__future__", names=[ast.alias("annotations")], level=0
            )
        )
        mod.body.append(
            ast.Import(names=[ast.alias("ast")]),
        )
        mod.body.append(ast.Import(names=[ast.alias("typing")], level=0))
        __all__ = ast.Assign(
            targets=[ast.Name(id="__all__", ctx=ast.Store())], value=ast.List(elts=[])
        )
        mod.body.append(__all__)
        all_names: List[str] = []
        for nid in self.order:
            n = self.nodes[nid]
            mod.body.append(n.to_classdef())
            all_names.append(n.id)
        __all__.value.elts = [ast.Constant(x) for x in sorted(all_names)]
        return mod

    def write_frontend_module(
        self, *, major: int, minor: int, backend_module: str, mixin_module: str
    ) -> ast.Module:
        mod = ast.Module(body=[], type_ignores=[])
        mod.body.append(
            ast.ImportFrom(
                module="__future__", names=[ast.alias("annotations")], level=0
            )
        )
        mod.body.append(
            ast.ImportFrom(
                module=".",
                names=[
                    ast.alias(name=f"_auto_ast{major}_{minor}", asname=backend_module)
                ],
                level=0,
            )
        )
        mod.body.append(
            ast.ImportFrom(
                module=".",
                names=[ast.alias(name=f"mixins", asname=mixin_module)],
                level=0,
            )
        )
        # mod.body.append(
        #     ast.Import(names=[ast.alias("ast")]),
        # )
        mod.body.append(ast.Import(names=[ast.alias("typing")], level=0))
        # mod.body.append(
        #     ast.AnnAssign(
        #         target=ast.Name("AST", ctx=ast.Store()),
        #         annotation=ast.Attribute(
        #             value=ast.Name("typing", ctx=ast.Load()),
        #             attr="TypeAlias",
        #             ctx=ast.Load(),
        #         ),
        #         value=ast.Attribute(
        #             value=ast.Name(backend_module),
        #             attr="AST",
        #             ctx=ast.Load(),
        #         ),
        #         simple=1,
        #     )
        # )
        __all__ = ast.Assign(
            targets=[ast.Name(id="__all__", ctx=ast.Store())], value=ast.List(elts=[])
        )
        mod.body.append(__all__)
        all_names: List[str] = []
        for nid in self.order:
            # if nid in ('AST',):
            #     continue
            n = self.nodes[nid]
            mod.body.append(
                n.to_frontend_classdef(
                    major=major,
                    minor=minor,
                    mixins=self.mixins.get(n.id, []),
                    backend_module=backend_module,
                    mixins_module=mixin_module,
                )
            )
            all_names.append(n.id)
        __all__.value.elts = [ast.Constant(x) for x in sorted(all_names)]
        return mod


def cmd_build(args: argparse.Namespace) -> None:
    NODE_DIR = Path("data") / "nodes"

    AST_DIR = Path("mkast") / "nodes"
    AST_DIR.mkdir(parents=True, exist_ok=True)
    (AST_DIR / "__init__.py").touch()

    for f in NODE_DIR.rglob("*~"):
        f.unlink()
    for f in AST_DIR.rglob("*~"):
        f.unlink()
    for f in AST_DIR.rglob("py*_*.py"):
        f.unlink()
    for f in AST_DIR.rglob("_auto_ast*_*.py"):
        f.unlink()

    idx: List[Path] = []
    for pyversion in SUPPORTED_PYVERSIONS:
        major, minor = pyversion
        node_version_dir = NODE_DIR / f"{major}.{minor}"
        ast_version_dir = AST_DIR / f"ast{major}_{minor}"

        mixins: Dict[str, Any] = {}
        with open(NODE_DIR / "mixin_assignments.yml", "r") as f:
            mixins = YAML.load(f)

        tree = Tree.LoadYAMLFrom(node_version_dir)
        tree.mixins = mixins

        backend_stem = f"_auto_ast{major}_{minor}"
        backend_file = (ast_version_dir / backend_stem).with_suffix(".py")

        mixin_stem = f"mixins"
        mixin_file = (ast_version_dir / mixin_stem).with_suffix(".py")

        frontend_stem = f"__init__"
        frontend_file = (ast_version_dir / frontend_stem).with_suffix(".py")

        write_python_to(tree.write_module(), backend_file)
        write_python_to(
            tree.write_frontend_module(
                major=major,
                minor=minor,
                backend_module=backend_stem,
                mixin_module=mixin_stem,
            ),
            frontend_file,
        )


def cmd_create_frontend(args: argparse.Namespace) -> None:
    major, minor = str(args.version).split(".")
    pyversion: Tuple[int, int] = (major, minor)
    NODE_DIR = Path("data") / "nodes"

    AST_DIR = Path("mkast") / "nodes"
    AST_DIR.mkdir(parents=True, exist_ok=True)
    (AST_DIR / "__init__.py").touch()

    for f in NODE_DIR.rglob("*~"):
        f.unlink()
    for f in AST_DIR.rglob("*~"):
        f.unlink()

    node_version_dir = NODE_DIR / f"{major}.{minor}"
    node_version_dir.mkdir(parents=True, exist_ok=True)
    ast_version_dir = AST_DIR / f"ast{major}_{minor}"
    ast_version_dir.mkdir(parents=True, exist_ok=True)

    backend_stem = f"_auto_ast{major}_{minor}"
    backend_file = (ast_version_dir / backend_stem).with_suffix(".py")

    mixin_stem = f"mixins"
    mixin_file = (ast_version_dir / mixin_stem).with_suffix(".py")

    frontend_stem = f"__init__"
    frontend_file = (ast_version_dir / frontend_stem).with_suffix(".py")

    mixins: Dict[str, Any] = {}
    with open(NODE_DIR / "mixin_assignments.yml", "r") as f:
        mixins = YAML.load(f)
    tree = Tree.LoadYAMLFrom(node_version_dir)
    tree.mixins = mixins

    if not mixin_file.is_file():
        mixin_file.write_text('sys.exit(f"TODO: Write mixins here. ({{__file__}})")\n')
    write_python_to(tree.write_module(), backend_file)
    write_python_to(
        tree.write_frontend_module(
            major=major, minor=minor, backend_module="autogen", mixin_module=mixin_stem
        ),
        frontend_file,
    )


def cmd_dump_all(_: argparse.Namespace) -> None:
    PYVI = get_version()
    NODE_DIR = Path("data") / "nodes"
    for subdir in NODE_DIR.iterdir():
        if subdir.is_dir():
            shutil.rmtree(subdir)

    AST_DIR = Path("mkast") / "nodes"
    AST_DIR.mkdir(parents=True, exist_ok=True)
    (AST_DIR / "__init__.py").touch()

    for f in NODE_DIR.rglob("*~"):
        f.unlink()
    for f in AST_DIR.rglob("*~"):
        f.unlink()

    idx: List[Path] = []
    order: List[str] = []
    for pyversion in SUPPORTED_PYVERSIONS:
        major, minor = pyversion
        node_version_dir = NODE_DIR / f"{major}.{minor}"
        node_version_dir.mkdir(parents=True, exist_ok=True)

        for f in node_version_dir.rglob("*.yml"):
            f.unlink()

        tree = Tree.ParseFrom(PATH_TYPESHED / "stdlib" / "_ast.pyi", pyversion)
        for n in tree.nodes.values():
            node_file: Path
            for i in range(5):
                suffix = "_" * i
                node_file = node_version_dir / f"{n.id.lower()}{suffix}.yml"
                if not node_file.is_file():
                    break
            write_yaml_to([{"fmt": FORMAT_VERSION}, n.serialize()], node_file)
            idx.append(node_file)
            if n.id not in order:
                order.append(n.id)

        write_yaml_to(
            [PYVI, {"files": sorted(map(str, idx)), "order": order}],
            node_version_dir / "__index.yml",
        )


def cmd_dump_ast(args: argparse.Namespace) -> None:
    major, minor = str(args.version).split(".")
    pyversion: Tuple[int, int] = (major, minor)

    PYVI = get_version()
    NODE_DIR = Path("data") / "nodes" / f"{major}.{minor}"
    if NODE_DIR.is_dir():
        shutil.rmtree(NODE_DIR)

    AST_DIR = Path("mkast") / "nodes"
    AST_DIR.mkdir(parents=True, exist_ok=True)
    (AST_DIR / "__init__.py").touch()

    for f in NODE_DIR.rglob("*~"):
        f.unlink()
    for f in AST_DIR.rglob("*~"):
        f.unlink()

    idx: List[Path] = []

    node_version_dir = NODE_DIR / f"{major}.{minor}"
    node_version_dir.mkdir(parents=True, exist_ok=True)

    for f in node_version_dir.rglob("*.yml"):
        f.unlink()
    ast_file = AST_DIR / f"py{major}_{minor}.py"
    tree = Tree.ParseFrom(PATH_TYPESHED / "stdlib" / "_ast.pyi", pyversion)
    order: List[str] = []
    for n in tree.nodes.values():
        node_file = node_version_dir / f"{n.id}.yml"
        write_yaml_to([{"fmt": FORMAT_VERSION}, n.serialize()], node_file)
        idx.append(node_file)
        if n.id not in order:
            order.append(n.id)

    write_yaml_to(
        [PYVI, {"files": sorted(map(str, idx)), "order": order}],
        node_version_dir / "__index.yml",
    )
    write_python_to(tree.write_module(), ast_file)


def main(argv: Optional[List[str]] = None) -> None:
    # if argv is None:
    #     argv = sys.argv

    import argparse

    argp = argparse.ArgumentParser()
    subp = argp.add_subparsers()

    p = subp.add_parser("build")
    p.set_defaults(cmd=cmd_build)

    p = subp.add_parser("create-frontend")
    p.add_argument("version", type=str)
    p.set_defaults(cmd=cmd_create_frontend)

    p = subp.add_parser("dump-ast")
    p.add_argument("version", type=str)
    p.set_defaults(cmd=cmd_dump_ast)

    p = subp.add_parser("dump-all")
    p.set_defaults(cmd=cmd_dump_all)

    args = argp.parse_args(args=argv)
    if hasattr(args, "cmd"):
        getattr(args, "cmd")(args)
    else:
        argp.print_usage()


if __name__ == "__main__":
    main()
