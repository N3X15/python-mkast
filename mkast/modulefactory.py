import ast
from typing import Iterable, List, Optional, Union

from mkast.ast_types import AnyAST
from mkast.utils import autobuild, value2ast


class ModuleFactory:
    def __init__(self) -> None:
        self.imports: List[Union[ast.Import, ast.ImportFrom]] = []
        self.expressions: List[ast.AST] = []

    def addImport(self, names: Iterable[str]) -> None:
        if isinstance(names, str):
            names = [names]
        self.imports.append(
            ast.Import(names=[ast.alias(name=x, asname=None) for x in names])
        )

    def addImportFrom(self, module: str, names: Iterable[str]) -> None:
        if isinstance(names, str):
            names = [names]
        self.imports.append(
            ast.ImportFrom(
                module=module,
                names=[ast.alias(name=x, asname=None) for x in names],
                level=0,
            )
        )

    def addVariableDecl(
        self,
        name: str,
        annotation: Optional[AnyAST],
        value: Optional[AnyAST],
        final: bool = False,
    ) -> None:
        annotation = autobuild(annotation)
        value = autobuild(value)

        n: ast.AST
        if final:
            self.addImportFrom("typing", ["Final"])
            if not annotation:
                annotation = ast.Name(id="Final", ctx=ast.Load())
            else:
                annotation = ast.Subscript(
                    value=ast.Name(id="Final", ctx=ast.Load()), slice=annotation
                )
        if annotation:
            n = ast.AnnAssign(
                target=ast.alias(name, asname=None),
                value=value2ast(value),
                annotation=annotation,
                simple=0,
            )
        else:
            n = ast.Assign(
                targets=[ast.alias(name, asname=None)],
                value=value2ast(value),
                type_comment=None,
                simple=0,
            )
        self.expressions.append(n)

    def generate(self) -> ast.Module:
        body = []
        for imp in self.imports:
            body.append(imp)
        for x in self.expressions:
            body.append(x)
        return ast.Module(body=body, type_ignores=[])
