from typing import Any
from . import _auto_ast3_11 as autogen


def value2ast(value: Any) -> autogen.AST:
    if isinstance(value, (int, float, str, bytes)):
        return autogen.Constant(value=value, kind=None)
    if isinstance(value, autogen.AST):
        return value
    return None
