from __future__ import annotations

import ast
import typing

__all__ = [
    "AST",
    "Add",
    "And",
    "AnnAssign",
    "Assert",
    "Assign",
    "AsyncFor",
    "AsyncFunctionDef",
    "AsyncWith",
    "Attribute",
    "AugAssign",
    "Await",
    "BinOp",
    "BitAnd",
    "BitOr",
    "BitXor",
    "BoolOp",
    "Break",
    "Call",
    "ClassDef",
    "Compare",
    "Constant",
    "Continue",
    "Del",
    "Delete",
    "Dict",
    "DictComp",
    "Div",
    "Eq",
    "ExceptHandler",
    "Expr",
    "Expression",
    "FloorDiv",
    "For",
    "FormattedValue",
    "FunctionDef",
    "FunctionType",
    "GeneratorExp",
    "Global",
    "Gt",
    "GtE",
    "If",
    "IfExp",
    "Import",
    "ImportFrom",
    "In",
    "Interactive",
    "Invert",
    "Is",
    "IsNot",
    "JoinedStr",
    "LShift",
    "Lambda",
    "List",
    "ListComp",
    "Load",
    "Lt",
    "LtE",
    "MatMult",
    "Match",
    "MatchAs",
    "MatchClass",
    "MatchMapping",
    "MatchOr",
    "MatchSequence",
    "MatchSingleton",
    "MatchStar",
    "MatchValue",
    "Mod",
    "Module",
    "Mult",
    "Name",
    "NamedExpr",
    "Nonlocal",
    "Not",
    "NotEq",
    "NotIn",
    "Or",
    "Pass",
    "Pow",
    "RShift",
    "Raise",
    "Return",
    "Set",
    "SetComp",
    "Slice",
    "Starred",
    "Store",
    "Sub",
    "Subscript",
    "Try",
    "TryStar",
    "Tuple",
    "TypeIgnore",
    "UAdd",
    "USub",
    "UnaryOp",
    "While",
    "With",
    "Yield",
    "YieldFrom",
    "alias",
    "arg",
    "arguments",
    "boolop",
    "cmpop",
    "comprehension",
    "excepthandler",
    "expr",
    "expr_context",
    "keyword",
    "match_case",
    "mod",
    "operator",
    "pattern",
    "stmt",
    "type_ignore",
    "unaryop",
    "withitem",
]


class AST:

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__()
        self.lineno: int = lineno
        self.col_offset: int = col_offset
        self.end_lineno: typing.Optional[int] = end_lineno
        self.end_col_offset: typing.Optional[int] = end_col_offset
        self.type_comment: typing.Optional[str] = type_comment

    def build(self) -> ast.AST:
        n: ast.AST = ast.AST()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class mod(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.mod:
        n: ast.mod = ast.mod()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class type_ignore(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.type_ignore:
        n: ast.type_ignore = ast.type_ignore()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class TypeIgnore(type_ignore):

    def __init__(
        self,
        *,
        tag: str = "",
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.tag: str = tag

    def build(self) -> ast.TypeIgnore:
        n: ast.TypeIgnore = ast.TypeIgnore()
        n.tag = self.tag
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class FunctionType(mod):

    def __init__(
        self,
        *,
        argtypes: list[expr] = None,
        returns: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.argtypes: list[expr] = argtypes
        self.returns: expr = returns

    def build(self) -> ast.FunctionType:
        n: ast.FunctionType = ast.FunctionType()
        n.argtypes = (
            [x.build() for x in self.argtypes] if self.argtypes is not None else None
        )
        n.returns = self.returns.build() if self.returns is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Module(mod):

    def __init__(
        self,
        *,
        body: list[stmt] = None,
        type_ignores: list[TypeIgnore] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.body: list[stmt] = body
        self.type_ignores: list[TypeIgnore] = type_ignores

    def build(self) -> ast.Module:
        n: ast.Module = ast.Module()
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.type_ignores = (
            [x.build() for x in self.type_ignores]
            if self.type_ignores is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Interactive(mod):

    def __init__(
        self,
        *,
        body: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.body: list[stmt] = body

    def build(self) -> ast.Interactive:
        n: ast.Interactive = ast.Interactive()
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Expression(mod):

    def __init__(
        self,
        *,
        body: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.body: expr = body

    def build(self) -> ast.Expression:
        n: ast.Expression = ast.Expression()
        n.body = self.body.build() if self.body is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class stmt(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.stmt:
        n: ast.stmt = ast.stmt()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class FunctionDef(stmt):

    def __init__(
        self,
        *,
        name: str = "",
        args: arguments = None,
        body: list[stmt] = None,
        decorator_list: list[expr] = None,
        returns: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.name: str = name
        self.args: arguments = args
        self.body: list[stmt] = body
        self.decorator_list: list[expr] = decorator_list
        self.returns: typing.Optional[expr] = returns

    def build(self) -> ast.FunctionDef:
        n: ast.FunctionDef = ast.FunctionDef()
        n.name = self.name
        n.args = self.args.build() if self.args is not None else None
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.decorator_list = (
            [x.build() for x in self.decorator_list]
            if self.decorator_list is not None
            else None
        )
        n.returns = (
            (self.returns.build() if isinstance(self.returns, AST) else self.returns)
            if self.returns is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class AsyncFunctionDef(stmt):

    def __init__(
        self,
        *,
        name: str = "",
        args: arguments = None,
        body: list[stmt] = None,
        decorator_list: list[expr] = None,
        returns: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.name: str = name
        self.args: arguments = args
        self.body: list[stmt] = body
        self.decorator_list: list[expr] = decorator_list
        self.returns: typing.Optional[expr] = returns

    def build(self) -> ast.AsyncFunctionDef:
        n: ast.AsyncFunctionDef = ast.AsyncFunctionDef()
        n.name = self.name
        n.args = self.args.build() if self.args is not None else None
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.decorator_list = (
            [x.build() for x in self.decorator_list]
            if self.decorator_list is not None
            else None
        )
        n.returns = (
            (self.returns.build() if isinstance(self.returns, AST) else self.returns)
            if self.returns is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class ClassDef(stmt):

    def __init__(
        self,
        *,
        name: str = "",
        bases: list[expr] = None,
        keywords: list[keyword] = None,
        body: list[stmt] = None,
        decorator_list: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.name: str = name
        self.bases: list[expr] = bases
        self.keywords: list[keyword] = keywords
        self.body: list[stmt] = body
        self.decorator_list: list[expr] = decorator_list

    def build(self) -> ast.ClassDef:
        n: ast.ClassDef = ast.ClassDef()
        n.name = self.name
        n.bases = [x.build() for x in self.bases] if self.bases is not None else None
        n.keywords = (
            [x.build() for x in self.keywords] if self.keywords is not None else None
        )
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.decorator_list = (
            [x.build() for x in self.decorator_list]
            if self.decorator_list is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Return(stmt):

    def __init__(
        self,
        *,
        value: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: typing.Optional[expr] = value

    def build(self) -> ast.Return:
        n: ast.Return = ast.Return()
        n.value = (
            (self.value.build() if isinstance(self.value, AST) else self.value)
            if self.value is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Delete(stmt):

    def __init__(
        self,
        *,
        targets: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.targets: list[expr] = targets

    def build(self) -> ast.Delete:
        n: ast.Delete = ast.Delete()
        n.targets = (
            [x.build() for x in self.targets] if self.targets is not None else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Assign(stmt):

    def __init__(
        self,
        *,
        targets: list[expr] = None,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.targets: list[expr] = targets
        self.value: expr = value

    def build(self) -> ast.Assign:
        n: ast.Assign = ast.Assign()
        n.targets = (
            [x.build() for x in self.targets] if self.targets is not None else None
        )
        n.value = self.value.build() if self.value is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class AugAssign(stmt):

    def __init__(
        self,
        *,
        target: typing.Union[Subscript, Name, Attribute] = None,
        op: operator = None,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.target: typing.Union[Subscript, Name, Attribute] = target
        self.op: operator = op
        self.value: expr = value

    def build(self) -> ast.AugAssign:
        n: ast.AugAssign = ast.AugAssign()
        n.target = self.target.build() if self.target is not None else None
        n.op = self.op.build() if self.op is not None else None
        n.value = self.value.build() if self.value is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class AnnAssign(stmt):

    def __init__(
        self,
        *,
        target: typing.Union[Subscript, Name, Attribute] = None,
        annotation: expr = None,
        value: typing.Optional[expr] = None,
        simple: int = 0,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.target: typing.Union[Subscript, Name, Attribute] = target
        self.annotation: expr = annotation
        self.value: typing.Optional[expr] = value
        self.simple: int = simple

    def build(self) -> ast.AnnAssign:
        n: ast.AnnAssign = ast.AnnAssign()
        n.target = self.target.build() if self.target is not None else None
        n.annotation = self.annotation.build() if self.annotation is not None else None
        n.value = (
            (self.value.build() if isinstance(self.value, AST) else self.value)
            if self.value is not None
            else None
        )
        n.simple = self.simple
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class For(stmt):

    def __init__(
        self,
        *,
        target: expr = None,
        iter: expr = None,
        body: list[stmt] = None,
        orelse: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.target: expr = target
        self.iter: expr = iter
        self.body: list[stmt] = body
        self.orelse: list[stmt] = orelse

    def build(self) -> ast.For:
        n: ast.For = ast.For()
        n.target = self.target.build() if self.target is not None else None
        n.iter = self.iter.build() if self.iter is not None else None
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.orelse = [x.build() for x in self.orelse] if self.orelse is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class AsyncFor(stmt):

    def __init__(
        self,
        *,
        target: expr = None,
        iter: expr = None,
        body: list[stmt] = None,
        orelse: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.target: expr = target
        self.iter: expr = iter
        self.body: list[stmt] = body
        self.orelse: list[stmt] = orelse

    def build(self) -> ast.AsyncFor:
        n: ast.AsyncFor = ast.AsyncFor()
        n.target = self.target.build() if self.target is not None else None
        n.iter = self.iter.build() if self.iter is not None else None
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.orelse = [x.build() for x in self.orelse] if self.orelse is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class While(stmt):

    def __init__(
        self,
        *,
        test: expr = None,
        body: list[stmt] = None,
        orelse: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.test: expr = test
        self.body: list[stmt] = body
        self.orelse: list[stmt] = orelse

    def build(self) -> ast.While:
        n: ast.While = ast.While()
        n.test = self.test.build() if self.test is not None else None
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.orelse = [x.build() for x in self.orelse] if self.orelse is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class If(stmt):

    def __init__(
        self,
        *,
        test: expr = None,
        body: list[stmt] = None,
        orelse: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.test: expr = test
        self.body: list[stmt] = body
        self.orelse: list[stmt] = orelse

    def build(self) -> ast.If:
        n: ast.If = ast.If()
        n.test = self.test.build() if self.test is not None else None
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.orelse = [x.build() for x in self.orelse] if self.orelse is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class With(stmt):

    def __init__(
        self,
        *,
        items: list[withitem] = None,
        body: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.items: list[withitem] = items
        self.body: list[stmt] = body

    def build(self) -> ast.With:
        n: ast.With = ast.With()
        n.items = [x.build() for x in self.items] if self.items is not None else None
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class AsyncWith(stmt):

    def __init__(
        self,
        *,
        items: list[withitem] = None,
        body: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.items: list[withitem] = items
        self.body: list[stmt] = body

    def build(self) -> ast.AsyncWith:
        n: ast.AsyncWith = ast.AsyncWith()
        n.items = [x.build() for x in self.items] if self.items is not None else None
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Raise(stmt):

    def __init__(
        self,
        *,
        exc: typing.Optional[expr] = None,
        cause: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.exc: typing.Optional[expr] = exc
        self.cause: typing.Optional[expr] = cause

    def build(self) -> ast.Raise:
        n: ast.Raise = ast.Raise()
        n.exc = (
            (self.exc.build() if isinstance(self.exc, AST) else self.exc)
            if self.exc is not None
            else None
        )
        n.cause = (
            (self.cause.build() if isinstance(self.cause, AST) else self.cause)
            if self.cause is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Try(stmt):

    def __init__(
        self,
        *,
        body: list[stmt] = None,
        handlers: list[ExceptHandler] = None,
        orelse: list[stmt] = None,
        finalbody: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.body: list[stmt] = body
        self.handlers: list[ExceptHandler] = handlers
        self.orelse: list[stmt] = orelse
        self.finalbody: list[stmt] = finalbody

    def build(self) -> ast.Try:
        n: ast.Try = ast.Try()
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.handlers = (
            [x.build() for x in self.handlers] if self.handlers is not None else None
        )
        n.orelse = [x.build() for x in self.orelse] if self.orelse is not None else None
        n.finalbody = (
            [x.build() for x in self.finalbody] if self.finalbody is not None else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Assert(stmt):

    def __init__(
        self,
        *,
        test: expr = None,
        msg: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.test: expr = test
        self.msg: typing.Optional[expr] = msg

    def build(self) -> ast.Assert:
        n: ast.Assert = ast.Assert()
        n.test = self.test.build() if self.test is not None else None
        n.msg = (
            (self.msg.build() if isinstance(self.msg, AST) else self.msg)
            if self.msg is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Import(stmt):

    def __init__(
        self,
        *,
        names: list[alias] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.names: list[alias] = names

    def build(self) -> ast.Import:
        n: ast.Import = ast.Import()
        n.names = [x.build() for x in self.names] if self.names is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class ImportFrom(stmt):

    def __init__(
        self,
        *,
        module: typing.Optional[str] = None,
        names: list[alias] = None,
        level: int = 0,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.module: typing.Optional[str] = module
        self.names: list[alias] = names
        self.level: int = level

    def build(self) -> ast.ImportFrom:
        n: ast.ImportFrom = ast.ImportFrom()
        n.module = self.module
        n.names = [x.build() for x in self.names] if self.names is not None else None
        n.level = self.level
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Global(stmt):

    def __init__(
        self,
        *,
        names: list[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.names: list[str] = names

    def build(self) -> ast.Global:
        n: ast.Global = ast.Global()
        n.names = self.names
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Nonlocal(stmt):

    def __init__(
        self,
        *,
        names: list[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.names: list[str] = names

    def build(self) -> ast.Nonlocal:
        n: ast.Nonlocal = ast.Nonlocal()
        n.names = self.names
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Expr(stmt):

    def __init__(
        self,
        *,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: expr = value

    def build(self) -> ast.Expr:
        n: ast.Expr = ast.Expr()
        n.value = self.value.build() if self.value is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Pass(stmt):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Pass:
        n: ast.Pass = ast.Pass()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Break(stmt):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Break:
        n: ast.Break = ast.Break()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Continue(stmt):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Continue:
        n: ast.Continue = ast.Continue()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class expr(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.expr:
        n: ast.expr = ast.expr()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class BoolOp(expr):

    def __init__(
        self,
        *,
        op: boolop = None,
        values: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.op: boolop = op
        self.values: list[expr] = values

    def build(self) -> ast.BoolOp:
        n: ast.BoolOp = ast.BoolOp()
        n.op = self.op.build() if self.op is not None else None
        n.values = [x.build() for x in self.values] if self.values is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class BinOp(expr):

    def __init__(
        self,
        *,
        left: expr = None,
        op: operator = None,
        right: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.left: expr = left
        self.op: operator = op
        self.right: expr = right

    def build(self) -> ast.BinOp:
        n: ast.BinOp = ast.BinOp()
        n.left = self.left.build() if self.left is not None else None
        n.op = self.op.build() if self.op is not None else None
        n.right = self.right.build() if self.right is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class UnaryOp(expr):

    def __init__(
        self,
        *,
        op: unaryop = None,
        operand: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.op: unaryop = op
        self.operand: expr = operand

    def build(self) -> ast.UnaryOp:
        n: ast.UnaryOp = ast.UnaryOp()
        n.op = self.op.build() if self.op is not None else None
        n.operand = self.operand.build() if self.operand is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Lambda(expr):

    def __init__(
        self,
        *,
        args: arguments = None,
        body: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.args: arguments = args
        self.body: expr = body

    def build(self) -> ast.Lambda:
        n: ast.Lambda = ast.Lambda()
        n.args = self.args.build() if self.args is not None else None
        n.body = self.body.build() if self.body is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class IfExp(expr):

    def __init__(
        self,
        *,
        test: expr = None,
        body: expr = None,
        orelse: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.test: expr = test
        self.body: expr = body
        self.orelse: expr = orelse

    def build(self) -> ast.IfExp:
        n: ast.IfExp = ast.IfExp()
        n.test = self.test.build() if self.test is not None else None
        n.body = self.body.build() if self.body is not None else None
        n.orelse = self.orelse.build() if self.orelse is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Dict(expr):

    def __init__(
        self,
        *,
        keys: list[expr | None] = None,
        values: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.keys: list[expr | None] = keys
        self.values: list[expr] = values

    def build(self) -> ast.Dict:
        n: ast.Dict = ast.Dict()
        n.keys = [x.build() for x in self.keys] if self.keys is not None else None
        n.values = [x.build() for x in self.values] if self.values is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Set(expr):

    def __init__(
        self,
        *,
        elts: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.elts: list[expr] = elts

    def build(self) -> ast.Set:
        n: ast.Set = ast.Set()
        n.elts = [x.build() for x in self.elts] if self.elts is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class ListComp(expr):

    def __init__(
        self,
        *,
        elt: expr = None,
        generators: list[comprehension] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.elt: expr = elt
        self.generators: list[comprehension] = generators

    def build(self) -> ast.ListComp:
        n: ast.ListComp = ast.ListComp()
        n.elt = self.elt.build() if self.elt is not None else None
        n.generators = (
            [x.build() for x in self.generators]
            if self.generators is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class SetComp(expr):

    def __init__(
        self,
        *,
        elt: expr = None,
        generators: list[comprehension] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.elt: expr = elt
        self.generators: list[comprehension] = generators

    def build(self) -> ast.SetComp:
        n: ast.SetComp = ast.SetComp()
        n.elt = self.elt.build() if self.elt is not None else None
        n.generators = (
            [x.build() for x in self.generators]
            if self.generators is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class DictComp(expr):

    def __init__(
        self,
        *,
        key: expr = None,
        value: expr = None,
        generators: list[comprehension] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.key: expr = key
        self.value: expr = value
        self.generators: list[comprehension] = generators

    def build(self) -> ast.DictComp:
        n: ast.DictComp = ast.DictComp()
        n.key = self.key.build() if self.key is not None else None
        n.value = self.value.build() if self.value is not None else None
        n.generators = (
            [x.build() for x in self.generators]
            if self.generators is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class GeneratorExp(expr):

    def __init__(
        self,
        *,
        elt: expr = None,
        generators: list[comprehension] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.elt: expr = elt
        self.generators: list[comprehension] = generators

    def build(self) -> ast.GeneratorExp:
        n: ast.GeneratorExp = ast.GeneratorExp()
        n.elt = self.elt.build() if self.elt is not None else None
        n.generators = (
            [x.build() for x in self.generators]
            if self.generators is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Await(expr):

    def __init__(
        self,
        *,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: expr = value

    def build(self) -> ast.Await:
        n: ast.Await = ast.Await()
        n.value = self.value.build() if self.value is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Yield(expr):

    def __init__(
        self,
        *,
        value: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: typing.Optional[expr] = value

    def build(self) -> ast.Yield:
        n: ast.Yield = ast.Yield()
        n.value = (
            (self.value.build() if isinstance(self.value, AST) else self.value)
            if self.value is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class YieldFrom(expr):

    def __init__(
        self,
        *,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: expr = value

    def build(self) -> ast.YieldFrom:
        n: ast.YieldFrom = ast.YieldFrom()
        n.value = self.value.build() if self.value is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Compare(expr):

    def __init__(
        self,
        *,
        left: expr = None,
        ops: list[cmpop] = None,
        comparators: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.left: expr = left
        self.ops: list[cmpop] = ops
        self.comparators: list[expr] = comparators

    def build(self) -> ast.Compare:
        n: ast.Compare = ast.Compare()
        n.left = self.left.build() if self.left is not None else None
        n.ops = [x.build() for x in self.ops] if self.ops is not None else None
        n.comparators = (
            [x.build() for x in self.comparators]
            if self.comparators is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Call(expr):

    def __init__(
        self,
        *,
        func: expr = None,
        args: list[expr] = None,
        keywords: list[keyword] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.func: expr = func
        self.args: list[expr] = args
        self.keywords: list[keyword] = keywords

    def build(self) -> ast.Call:
        n: ast.Call = ast.Call()
        n.func = self.func.build() if self.func is not None else None
        n.args = [x.build() for x in self.args] if self.args is not None else None
        n.keywords = (
            [x.build() for x in self.keywords] if self.keywords is not None else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class FormattedValue(expr):

    def __init__(
        self,
        *,
        value: expr = None,
        conversion: int = 0,
        format_spec: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: expr = value
        self.conversion: int = conversion
        self.format_spec: typing.Optional[expr] = format_spec

    def build(self) -> ast.FormattedValue:
        n: ast.FormattedValue = ast.FormattedValue()
        n.value = self.value.build() if self.value is not None else None
        n.conversion = self.conversion
        n.format_spec = (
            (
                self.format_spec.build()
                if isinstance(self.format_spec, AST)
                else self.format_spec
            )
            if self.format_spec is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class JoinedStr(expr):

    def __init__(
        self,
        *,
        values: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.values: list[expr] = values

    def build(self) -> ast.JoinedStr:
        n: ast.JoinedStr = ast.JoinedStr()
        n.values = [x.build() for x in self.values] if self.values is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Constant(expr):

    def __init__(
        self,
        *,
        value: typing.Any = None,
        kind: typing.Optional[str] = None,
        s: typing.Any = None,
        n: typing.Union[complex, int, float] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: typing.Any = value
        self.kind: typing.Optional[str] = kind
        self.s: typing.Any = s
        self.n: typing.Union[complex, int, float] = n

    def build(self) -> ast.Constant:
        n: ast.Constant = ast.Constant()
        n.value = self.value
        n.kind = self.kind
        n.s = self.s
        n.n = self.n.build() if self.n is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class NamedExpr(expr):

    def __init__(
        self,
        *,
        target: Name = None,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.target: Name = target
        self.value: expr = value

    def build(self) -> ast.NamedExpr:
        n: ast.NamedExpr = ast.NamedExpr()
        n.target = self.target.build() if self.target is not None else None
        n.value = self.value.build() if self.value is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Attribute(expr):

    def __init__(
        self,
        *,
        value: expr = None,
        attr: str = "",
        ctx: expr_context = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: expr = value
        self.attr: str = attr
        self.ctx: expr_context = ctx

    def build(self) -> ast.Attribute:
        n: ast.Attribute = ast.Attribute()
        n.value = self.value.build() if self.value is not None else None
        n.attr = self.attr
        n.ctx = self.ctx.build() if self.ctx is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Slice(expr):

    def __init__(
        self,
        *,
        lower: typing.Optional[expr] = None,
        upper: typing.Optional[expr] = None,
        step: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.lower: typing.Optional[expr] = lower
        self.upper: typing.Optional[expr] = upper
        self.step: typing.Optional[expr] = step

    def build(self) -> ast.Slice:
        n: ast.Slice = ast.Slice()
        n.lower = (
            (self.lower.build() if isinstance(self.lower, AST) else self.lower)
            if self.lower is not None
            else None
        )
        n.upper = (
            (self.upper.build() if isinstance(self.upper, AST) else self.upper)
            if self.upper is not None
            else None
        )
        n.step = (
            (self.step.build() if isinstance(self.step, AST) else self.step)
            if self.step is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Subscript(expr):

    def __init__(
        self,
        *,
        value: expr = None,
        slice: expr = None,
        ctx: expr_context = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: expr = value
        self.slice: expr = slice
        self.ctx: expr_context = ctx

    def build(self) -> ast.Subscript:
        n: ast.Subscript = ast.Subscript()
        n.value = self.value.build() if self.value is not None else None
        n.slice = self.slice.build() if self.slice is not None else None
        n.ctx = self.ctx.build() if self.ctx is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Starred(expr):

    def __init__(
        self,
        *,
        value: expr = None,
        ctx: expr_context = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: expr = value
        self.ctx: expr_context = ctx

    def build(self) -> ast.Starred:
        n: ast.Starred = ast.Starred()
        n.value = self.value.build() if self.value is not None else None
        n.ctx = self.ctx.build() if self.ctx is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Name(expr):

    def __init__(
        self,
        *,
        id: str = "",
        ctx: expr_context = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.id: str = id
        self.ctx: expr_context = ctx

    def build(self) -> ast.Name:
        n: ast.Name = ast.Name()
        n.id = self.id
        n.ctx = self.ctx.build() if self.ctx is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class List(expr):

    def __init__(
        self,
        *,
        elts: list[expr] = None,
        ctx: expr_context = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.elts: list[expr] = elts
        self.ctx: expr_context = ctx

    def build(self) -> ast.List:
        n: ast.List = ast.List()
        n.elts = [x.build() for x in self.elts] if self.elts is not None else None
        n.ctx = self.ctx.build() if self.ctx is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Tuple(expr):

    def __init__(
        self,
        *,
        elts: list[expr] = None,
        ctx: expr_context = None,
        dims: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.elts: list[expr] = elts
        self.ctx: expr_context = ctx
        self.dims: list[expr] = dims

    def build(self) -> ast.Tuple:
        n: ast.Tuple = ast.Tuple()
        n.elts = [x.build() for x in self.elts] if self.elts is not None else None
        n.ctx = self.ctx.build() if self.ctx is not None else None
        n.dims = [x.build() for x in self.dims] if self.dims is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class expr_context(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.expr_context:
        n: ast.expr_context = ast.expr_context()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Del(expr_context):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Del:
        n: ast.Del = ast.Del()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Load(expr_context):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Load:
        n: ast.Load = ast.Load()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Store(expr_context):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Store:
        n: ast.Store = ast.Store()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class boolop(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.boolop:
        n: ast.boolop = ast.boolop()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class And(boolop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.And:
        n: ast.And = ast.And()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Or(boolop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Or:
        n: ast.Or = ast.Or()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class operator(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.operator:
        n: ast.operator = ast.operator()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Add(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Add:
        n: ast.Add = ast.Add()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class BitAnd(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.BitAnd:
        n: ast.BitAnd = ast.BitAnd()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class BitOr(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.BitOr:
        n: ast.BitOr = ast.BitOr()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class BitXor(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.BitXor:
        n: ast.BitXor = ast.BitXor()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Div(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Div:
        n: ast.Div = ast.Div()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class FloorDiv(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.FloorDiv:
        n: ast.FloorDiv = ast.FloorDiv()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class LShift(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.LShift:
        n: ast.LShift = ast.LShift()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Mod(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Mod:
        n: ast.Mod = ast.Mod()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Mult(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Mult:
        n: ast.Mult = ast.Mult()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class MatMult(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.MatMult:
        n: ast.MatMult = ast.MatMult()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Pow(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Pow:
        n: ast.Pow = ast.Pow()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class RShift(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.RShift:
        n: ast.RShift = ast.RShift()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Sub(operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Sub:
        n: ast.Sub = ast.Sub()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class unaryop(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.unaryop:
        n: ast.unaryop = ast.unaryop()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Invert(unaryop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Invert:
        n: ast.Invert = ast.Invert()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Not(unaryop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Not:
        n: ast.Not = ast.Not()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class UAdd(unaryop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.UAdd:
        n: ast.UAdd = ast.UAdd()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class USub(unaryop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.USub:
        n: ast.USub = ast.USub()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class cmpop(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.cmpop:
        n: ast.cmpop = ast.cmpop()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Eq(cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Eq:
        n: ast.Eq = ast.Eq()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Gt(cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Gt:
        n: ast.Gt = ast.Gt()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class GtE(cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.GtE:
        n: ast.GtE = ast.GtE()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class In(cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.In:
        n: ast.In = ast.In()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Is(cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Is:
        n: ast.Is = ast.Is()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class IsNot(cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.IsNot:
        n: ast.IsNot = ast.IsNot()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Lt(cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.Lt:
        n: ast.Lt = ast.Lt()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class LtE(cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.LtE:
        n: ast.LtE = ast.LtE()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class NotEq(cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.NotEq:
        n: ast.NotEq = ast.NotEq()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class NotIn(cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.NotIn:
        n: ast.NotIn = ast.NotIn()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class comprehension(AST):

    def __init__(
        self,
        *,
        target: expr = None,
        iter: expr = None,
        ifs: list[expr] = None,
        is_async: int = 0,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.target: expr = target
        self.iter: expr = iter
        self.ifs: list[expr] = ifs
        self.is_async: int = is_async

    def build(self) -> ast.comprehension:
        n: ast.comprehension = ast.comprehension()
        n.target = self.target.build() if self.target is not None else None
        n.iter = self.iter.build() if self.iter is not None else None
        n.ifs = [x.build() for x in self.ifs] if self.ifs is not None else None
        n.is_async = self.is_async
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class excepthandler(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.excepthandler:
        n: ast.excepthandler = ast.excepthandler()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class ExceptHandler(excepthandler):

    def __init__(
        self,
        *,
        type: typing.Optional[expr] = None,
        name: typing.Optional[str] = None,
        body: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.type: typing.Optional[expr] = type
        self.name: typing.Optional[str] = name
        self.body: list[stmt] = body

    def build(self) -> ast.ExceptHandler:
        n: ast.ExceptHandler = ast.ExceptHandler()
        n.type = (
            (self.type.build() if isinstance(self.type, AST) else self.type)
            if self.type is not None
            else None
        )
        n.name = self.name
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class arguments(AST):

    def __init__(
        self,
        *,
        posonlyargs: list[arg] = None,
        args: list[arg] = None,
        vararg: typing.Optional[arg] = None,
        kwonlyargs: list[arg] = None,
        kw_defaults: list[expr | None] = None,
        kwarg: typing.Optional[arg] = None,
        defaults: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.posonlyargs: list[arg] = posonlyargs
        self.args: list[arg] = args
        self.vararg: typing.Optional[arg] = vararg
        self.kwonlyargs: list[arg] = kwonlyargs
        self.kw_defaults: list[expr | None] = kw_defaults
        self.kwarg: typing.Optional[arg] = kwarg
        self.defaults: list[expr] = defaults

    def build(self) -> ast.arguments:
        n: ast.arguments = ast.arguments()
        n.posonlyargs = (
            [x.build() for x in self.posonlyargs]
            if self.posonlyargs is not None
            else None
        )
        n.args = [x.build() for x in self.args] if self.args is not None else None
        n.vararg = (
            (self.vararg.build() if isinstance(self.vararg, AST) else self.vararg)
            if self.vararg is not None
            else None
        )
        n.kwonlyargs = (
            [x.build() for x in self.kwonlyargs]
            if self.kwonlyargs is not None
            else None
        )
        n.kw_defaults = (
            [x.build() for x in self.kw_defaults]
            if self.kw_defaults is not None
            else None
        )
        n.kwarg = (
            (self.kwarg.build() if isinstance(self.kwarg, AST) else self.kwarg)
            if self.kwarg is not None
            else None
        )
        n.defaults = (
            [x.build() for x in self.defaults] if self.defaults is not None else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class arg(AST):

    def __init__(
        self,
        *,
        arg: str = "",
        annotation: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.arg: str = arg
        self.annotation: typing.Optional[expr] = annotation

    def build(self) -> ast.arg:
        n: ast.arg = ast.arg()
        n.arg = self.arg
        n.annotation = (
            (
                self.annotation.build()
                if isinstance(self.annotation, AST)
                else self.annotation
            )
            if self.annotation is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class keyword(AST):

    def __init__(
        self,
        *,
        arg: typing.Optional[str] = None,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.arg: typing.Optional[str] = arg
        self.value: expr = value

    def build(self) -> ast.keyword:
        n: ast.keyword = ast.keyword()
        n.arg = self.arg
        n.value = self.value.build() if self.value is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class alias(AST):

    def __init__(
        self,
        *,
        name: str = "",
        asname: typing.Optional[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.name: str = name
        self.asname: typing.Optional[str] = asname

    def build(self) -> ast.alias:
        n: ast.alias = ast.alias()
        n.name = self.name
        n.asname = self.asname
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class withitem(AST):

    def __init__(
        self,
        *,
        context_expr: expr = None,
        optional_vars: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.context_expr: expr = context_expr
        self.optional_vars: typing.Optional[expr] = optional_vars

    def build(self) -> ast.withitem:
        n: ast.withitem = ast.withitem()
        n.context_expr = (
            self.context_expr.build() if self.context_expr is not None else None
        )
        n.optional_vars = (
            (
                self.optional_vars.build()
                if isinstance(self.optional_vars, AST)
                else self.optional_vars
            )
            if self.optional_vars is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class Match(stmt):

    def __init__(
        self,
        *,
        subject: expr = None,
        cases: list[match_case] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.subject: expr = subject
        self.cases: list[match_case] = cases

    def build(self) -> ast.Match:
        n: ast.Match = ast.Match()
        n.subject = self.subject.build() if self.subject is not None else None
        n.cases = [x.build() for x in self.cases] if self.cases is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class pattern(AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )

    def build(self) -> ast.pattern:
        n: ast.pattern = ast.pattern()
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class match_case(AST):

    def __init__(
        self,
        *,
        pattern: typing.Pattern = None,
        guard: typing.Optional[expr] = None,
        body: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.pattern: typing.Pattern = pattern
        self.guard: typing.Optional[expr] = guard
        self.body: list[stmt] = body

    def build(self) -> ast.match_case:
        n: ast.match_case = ast.match_case()
        n.pattern = self.pattern.build() if self.pattern is not None else None
        n.guard = (
            (self.guard.build() if isinstance(self.guard, AST) else self.guard)
            if self.guard is not None
            else None
        )
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class MatchValue(pattern):

    def __init__(
        self,
        *,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: expr = value

    def build(self) -> ast.MatchValue:
        n: ast.MatchValue = ast.MatchValue()
        n.value = self.value.build() if self.value is not None else None
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class MatchSingleton(pattern):

    def __init__(
        self,
        *,
        value: typing.Optional[typing.Literal[True, False]] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.value: typing.Optional[typing.Literal[True, False]] = value

    def build(self) -> ast.MatchSingleton:
        n: ast.MatchSingleton = ast.MatchSingleton()
        n.value = (
            (self.value.build() if isinstance(self.value, AST) else self.value)
            if self.value is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class MatchSequence(pattern):

    def __init__(
        self,
        *,
        patterns: list[pattern] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.patterns: list[pattern] = patterns

    def build(self) -> ast.MatchSequence:
        n: ast.MatchSequence = ast.MatchSequence()
        n.patterns = (
            [x.build() for x in self.patterns] if self.patterns is not None else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class MatchStar(pattern):

    def __init__(
        self,
        *,
        name: typing.Optional[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.name: typing.Optional[str] = name

    def build(self) -> ast.MatchStar:
        n: ast.MatchStar = ast.MatchStar()
        n.name = self.name
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class MatchMapping(pattern):

    def __init__(
        self,
        *,
        keys: list[expr] = None,
        patterns: list[pattern] = None,
        rest: typing.Optional[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.keys: list[expr] = keys
        self.patterns: list[pattern] = patterns
        self.rest: typing.Optional[str] = rest

    def build(self) -> ast.MatchMapping:
        n: ast.MatchMapping = ast.MatchMapping()
        n.keys = [x.build() for x in self.keys] if self.keys is not None else None
        n.patterns = (
            [x.build() for x in self.patterns] if self.patterns is not None else None
        )
        n.rest = self.rest
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class MatchClass(pattern):

    def __init__(
        self,
        *,
        cls: expr = None,
        patterns: list[pattern] = None,
        kwd_attrs: list[str] = None,
        kwd_patterns: list[pattern] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.cls: expr = cls
        self.patterns: list[pattern] = patterns
        self.kwd_attrs: list[str] = kwd_attrs
        self.kwd_patterns: list[pattern] = kwd_patterns

    def build(self) -> ast.MatchClass:
        n: ast.MatchClass = ast.MatchClass()
        n.cls = self.cls.build() if self.cls is not None else None
        n.patterns = (
            [x.build() for x in self.patterns] if self.patterns is not None else None
        )
        n.kwd_attrs = self.kwd_attrs
        n.kwd_patterns = (
            [x.build() for x in self.kwd_patterns]
            if self.kwd_patterns is not None
            else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class MatchAs(pattern):

    def __init__(
        self,
        *,
        pattern: typing.Optional[typing.Pattern] = None,
        name: typing.Optional[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.pattern: typing.Optional[typing.Pattern] = pattern
        self.name: typing.Optional[str] = name

    def build(self) -> ast.MatchAs:
        n: ast.MatchAs = ast.MatchAs()
        n.pattern = (
            (self.pattern.build() if isinstance(self.pattern, AST) else self.pattern)
            if self.pattern is not None
            else None
        )
        n.name = self.name
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class MatchOr(pattern):

    def __init__(
        self,
        *,
        patterns: list[pattern] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.patterns: list[pattern] = patterns

    def build(self) -> ast.MatchOr:
        n: ast.MatchOr = ast.MatchOr()
        n.patterns = (
            [x.build() for x in self.patterns] if self.patterns is not None else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n


class TryStar(stmt):

    def __init__(
        self,
        *,
        body: list[stmt] = None,
        handlers: list[ExceptHandler] = None,
        orelse: list[stmt] = None,
        finalbody: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
        self.body: list[stmt] = body
        self.handlers: list[ExceptHandler] = handlers
        self.orelse: list[stmt] = orelse
        self.finalbody: list[stmt] = finalbody

    def build(self) -> ast.TryStar:
        n: ast.TryStar = ast.TryStar()
        n.body = [x.build() for x in self.body] if self.body is not None else None
        n.handlers = (
            [x.build() for x in self.handlers] if self.handlers is not None else None
        )
        n.orelse = [x.build() for x in self.orelse] if self.orelse is not None else None
        n.finalbody = (
            [x.build() for x in self.finalbody] if self.finalbody is not None else None
        )
        n.lineno = self.lineno
        n.col_offset = self.col_offset
        n.end_lineno = self.end_lineno
        n.end_col_offset = self.end_col_offset
        n.type_comment = self.type_comment
        return n
