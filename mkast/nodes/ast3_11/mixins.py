# Do NOT use stock 'ast' module here.

from abc import ABC, abstractmethod
from typing import Any, Iterable, Optional

from .utils import value2ast

from . import _auto_ast3_11 as autogen


class IBodiedClass(ABC):
    @abstractmethod
    def _getBody(self) -> list[autogen.stmt]:
        ...


class AddImportsMixin(IBodiedClass):
    def addImport(self, names: Iterable[str]) -> None:
        if isinstance(names, str):
            names = [names]
        self._getBody().append(
            autogen.Import(names=[autogen.alias(name=x, asname=None) for x in names])
        )

    def addImportFrom(self, module: str, names: Iterable[str]) -> None:
        if isinstance(names, str):
            names = [names]
        self._getBody().append(
            autogen.ImportFrom(
                module=module,
                names=[autogen.alias(name=x, asname=None) for x in names],
                level=0,
            )
        )


class AddVariableMixin(IBodiedClass):
    def addVariableDecl(
        self,
        name: str,
        annotation: Optional[autogen.AST],
        value: Any,
        final: bool = False,
    ) -> None:
        n: autogen.stmt
        if final:
            # self.addImportFrom("typing", ["Final"])
            if not annotation:
                annotation = autogen.Name(id="Final", ctx=autogen.Load())
            else:
                annotation = autogen.Subscript(
                    value=autogen.Name(id="Final", ctx=autogen.Load()), slice=n
                )
        if annotation:
            n = autogen.AnnAssign(
                target=autogen.alias(name, asname=None),
                value=value2ast(value),
                annotation=annotation,
                simple=0,
            )
        else:
            n = autogen.Assign(
                targets=[autogen.alias(name, asname=None)],
                value=value2ast(value),
                type_comment=None,
                simple=0,
            )
        self._getBody().append(n)
