import importlib
import sys
from types import ModuleType
from typing import Optional


def getNodes(
    version_major: Optional[int] = None, version_minor: Optional[int] = None
) -> ModuleType:
    return importlib.import_module(
        name=f"ast{version_major or sys.version_info.major}_{version_minor or sys.version_info.minor}",
        package=".",
    )
