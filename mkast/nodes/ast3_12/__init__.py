from __future__ import annotations

import typing

from . import _auto_ast3_12 as _auto_ast3_12
from . import mixins as mixins

__all__ = [
    "AST",
    "Add",
    "And",
    "AnnAssign",
    "Assert",
    "Assign",
    "AsyncFor",
    "AsyncFunctionDef",
    "AsyncWith",
    "Attribute",
    "AugAssign",
    "Await",
    "BinOp",
    "BitAnd",
    "BitOr",
    "BitXor",
    "BoolOp",
    "Break",
    "Call",
    "ClassDef",
    "Compare",
    "Constant",
    "Continue",
    "Del",
    "Delete",
    "Dict",
    "DictComp",
    "Div",
    "Eq",
    "ExceptHandler",
    "Expr",
    "Expression",
    "FloorDiv",
    "For",
    "FormattedValue",
    "FunctionDef",
    "FunctionType",
    "GeneratorExp",
    "Global",
    "Gt",
    "GtE",
    "If",
    "IfExp",
    "Import",
    "ImportFrom",
    "In",
    "Interactive",
    "Invert",
    "Is",
    "IsNot",
    "JoinedStr",
    "LShift",
    "Lambda",
    "List",
    "ListComp",
    "Load",
    "Lt",
    "LtE",
    "MatMult",
    "Match",
    "MatchAs",
    "MatchClass",
    "MatchMapping",
    "MatchOr",
    "MatchSequence",
    "MatchSingleton",
    "MatchStar",
    "MatchValue",
    "Mod",
    "Module",
    "Mult",
    "Name",
    "NamedExpr",
    "Nonlocal",
    "Not",
    "NotEq",
    "NotIn",
    "Or",
    "ParamSpec",
    "Pass",
    "Pow",
    "RShift",
    "Raise",
    "Return",
    "Set",
    "SetComp",
    "Slice",
    "Starred",
    "Store",
    "Sub",
    "Subscript",
    "Try",
    "TryStar",
    "Tuple",
    "TypeAlias",
    "TypeIgnore",
    "TypeVar",
    "TypeVarTuple",
    "UAdd",
    "USub",
    "UnaryOp",
    "While",
    "With",
    "Yield",
    "YieldFrom",
    "alias",
    "arg",
    "arguments",
    "boolop",
    "cmpop",
    "comprehension",
    "excepthandler",
    "expr",
    "expr_context",
    "keyword",
    "match_case",
    "mod",
    "operator",
    "pattern",
    "stmt",
    "type_ignore",
    "type_param",
    "unaryop",
    "withitem",
]


class AST(_auto_ast3_12.AST):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class mod(_auto_ast3_12.mod):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class type_ignore(_auto_ast3_12.type_ignore):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class TypeIgnore(_auto_ast3_12.TypeIgnore):

    def __init__(
        self,
        *,
        tag: str = "",
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            tag=tag,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class FunctionType(_auto_ast3_12.FunctionType):

    def __init__(
        self,
        *,
        argtypes: list[expr] = None,
        returns: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            argtypes=argtypes,
            returns=returns,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Module(mixins.AddImportsMixin, mixins.AddVariableMixin, _auto_ast3_12.Module):

    def __init__(
        self,
        *,
        body: list[stmt] = None,
        type_ignores: list[TypeIgnore] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            body=body,
            type_ignores=type_ignores,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Interactive(
    mixins.AddImportsMixin, mixins.AddVariableMixin, _auto_ast3_12.Interactive
):

    def __init__(
        self,
        *,
        body: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            body=body,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Expression(_auto_ast3_12.Expression):

    def __init__(
        self,
        *,
        body: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            body=body,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class stmt(_auto_ast3_12.stmt):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class FunctionDef(
    mixins.AddImportsMixin, mixins.AddVariableMixin, _auto_ast3_12.FunctionDef
):

    def __init__(
        self,
        *,
        name: str = "",
        args: arguments = None,
        body: list[stmt] = None,
        decorator_list: list[expr] = None,
        returns: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            name=name,
            args=args,
            body=body,
            decorator_list=decorator_list,
            returns=returns,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class AsyncFunctionDef(
    mixins.AddImportsMixin, mixins.AddVariableMixin, _auto_ast3_12.AsyncFunctionDef
):

    def __init__(
        self,
        *,
        name: str = "",
        args: arguments = None,
        body: list[stmt] = None,
        decorator_list: list[expr] = None,
        returns: typing.Optional[expr] = None,
        type_params: list[type_param] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            name=name,
            args=args,
            body=body,
            decorator_list=decorator_list,
            returns=returns,
            type_params=type_params,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class ClassDef(mixins.AddVariableMixin, _auto_ast3_12.ClassDef):

    def __init__(
        self,
        *,
        name: str = "",
        bases: list[expr] = None,
        keywords: list[keyword] = None,
        body: list[stmt] = None,
        decorator_list: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            name=name,
            bases=bases,
            keywords=keywords,
            body=body,
            decorator_list=decorator_list,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Return(_auto_ast3_12.Return):

    def __init__(
        self,
        *,
        value: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Delete(_auto_ast3_12.Delete):

    def __init__(
        self,
        *,
        targets: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            targets=targets,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Assign(_auto_ast3_12.Assign):

    def __init__(
        self,
        *,
        targets: list[expr] = None,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            targets=targets,
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class AugAssign(_auto_ast3_12.AugAssign):

    def __init__(
        self,
        *,
        target: typing.Union[Subscript, Name, Attribute] = None,
        op: operator = None,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            target=target,
            op=op,
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class AnnAssign(_auto_ast3_12.AnnAssign):

    def __init__(
        self,
        *,
        target: typing.Union[Subscript, Name, Attribute] = None,
        annotation: expr = None,
        value: typing.Optional[expr] = None,
        simple: int = 0,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            target=target,
            annotation=annotation,
            value=value,
            simple=simple,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class For(_auto_ast3_12.For):

    def __init__(
        self,
        *,
        target: expr = None,
        iter: expr = None,
        body: list[stmt] = None,
        orelse: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            target=target,
            iter=iter,
            body=body,
            orelse=orelse,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class AsyncFor(_auto_ast3_12.AsyncFor):

    def __init__(
        self,
        *,
        target: expr = None,
        iter: expr = None,
        body: list[stmt] = None,
        orelse: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            target=target,
            iter=iter,
            body=body,
            orelse=orelse,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class While(_auto_ast3_12.While):

    def __init__(
        self,
        *,
        test: expr = None,
        body: list[stmt] = None,
        orelse: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            test=test,
            body=body,
            orelse=orelse,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class If(_auto_ast3_12.If):

    def __init__(
        self,
        *,
        test: expr = None,
        body: list[stmt] = None,
        orelse: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            test=test,
            body=body,
            orelse=orelse,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class With(_auto_ast3_12.With):

    def __init__(
        self,
        *,
        items: list[withitem] = None,
        body: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            items=items,
            body=body,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class AsyncWith(_auto_ast3_12.AsyncWith):

    def __init__(
        self,
        *,
        items: list[withitem] = None,
        body: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            items=items,
            body=body,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Raise(_auto_ast3_12.Raise):

    def __init__(
        self,
        *,
        exc: typing.Optional[expr] = None,
        cause: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            exc=exc,
            cause=cause,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Try(_auto_ast3_12.Try):

    def __init__(
        self,
        *,
        body: list[stmt] = None,
        handlers: list[ExceptHandler] = None,
        orelse: list[stmt] = None,
        finalbody: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            body=body,
            handlers=handlers,
            orelse=orelse,
            finalbody=finalbody,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Assert(_auto_ast3_12.Assert):

    def __init__(
        self,
        *,
        test: expr = None,
        msg: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            test=test,
            msg=msg,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Import(_auto_ast3_12.Import):

    def __init__(
        self,
        *,
        names: list[alias] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            names=names,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class ImportFrom(_auto_ast3_12.ImportFrom):

    def __init__(
        self,
        *,
        module: typing.Optional[str] = None,
        names: list[alias] = None,
        level: int = 0,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            module=module,
            names=names,
            level=level,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Global(_auto_ast3_12.Global):

    def __init__(
        self,
        *,
        names: list[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            names=names,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Nonlocal(_auto_ast3_12.Nonlocal):

    def __init__(
        self,
        *,
        names: list[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            names=names,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Expr(_auto_ast3_12.Expr):

    def __init__(
        self,
        *,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Pass(_auto_ast3_12.Pass):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Break(_auto_ast3_12.Break):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Continue(_auto_ast3_12.Continue):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class expr(_auto_ast3_12.expr):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class BoolOp(_auto_ast3_12.BoolOp):

    def __init__(
        self,
        *,
        op: boolop = None,
        values: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            op=op,
            values=values,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class BinOp(_auto_ast3_12.BinOp):

    def __init__(
        self,
        *,
        left: expr = None,
        op: operator = None,
        right: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            left=left,
            op=op,
            right=right,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class UnaryOp(_auto_ast3_12.UnaryOp):

    def __init__(
        self,
        *,
        op: unaryop = None,
        operand: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            op=op,
            operand=operand,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Lambda(_auto_ast3_12.Lambda):

    def __init__(
        self,
        *,
        args: arguments = None,
        body: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            args=args,
            body=body,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class IfExp(_auto_ast3_12.IfExp):

    def __init__(
        self,
        *,
        test: expr = None,
        body: expr = None,
        orelse: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            test=test,
            body=body,
            orelse=orelse,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Dict(_auto_ast3_12.Dict):

    def __init__(
        self,
        *,
        keys: list[expr | None] = None,
        values: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            keys=keys,
            values=values,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Set(_auto_ast3_12.Set):

    def __init__(
        self,
        *,
        elts: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            elts=elts,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class ListComp(_auto_ast3_12.ListComp):

    def __init__(
        self,
        *,
        elt: expr = None,
        generators: list[comprehension] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            elt=elt,
            generators=generators,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class SetComp(_auto_ast3_12.SetComp):

    def __init__(
        self,
        *,
        elt: expr = None,
        generators: list[comprehension] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            elt=elt,
            generators=generators,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class DictComp(_auto_ast3_12.DictComp):

    def __init__(
        self,
        *,
        key: expr = None,
        value: expr = None,
        generators: list[comprehension] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            key=key,
            value=value,
            generators=generators,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class GeneratorExp(_auto_ast3_12.GeneratorExp):

    def __init__(
        self,
        *,
        elt: expr = None,
        generators: list[comprehension] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            elt=elt,
            generators=generators,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Await(_auto_ast3_12.Await):

    def __init__(
        self,
        *,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Yield(_auto_ast3_12.Yield):

    def __init__(
        self,
        *,
        value: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class YieldFrom(_auto_ast3_12.YieldFrom):

    def __init__(
        self,
        *,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Compare(_auto_ast3_12.Compare):

    def __init__(
        self,
        *,
        left: expr = None,
        ops: list[cmpop] = None,
        comparators: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            left=left,
            ops=ops,
            comparators=comparators,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Call(_auto_ast3_12.Call):

    def __init__(
        self,
        *,
        func: expr = None,
        args: list[expr] = None,
        keywords: list[keyword] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            func=func,
            args=args,
            keywords=keywords,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class FormattedValue(_auto_ast3_12.FormattedValue):

    def __init__(
        self,
        *,
        value: expr = None,
        conversion: int = 0,
        format_spec: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            conversion=conversion,
            format_spec=format_spec,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class JoinedStr(_auto_ast3_12.JoinedStr):

    def __init__(
        self,
        *,
        values: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            values=values,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Constant(_auto_ast3_12.Constant):

    def __init__(
        self,
        *,
        value: typing.Any = None,
        kind: typing.Optional[str] = None,
        s: typing.Any = None,
        n: typing.Union[complex, int, float] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            kind=kind,
            s=s,
            n=n,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class NamedExpr(_auto_ast3_12.NamedExpr):

    def __init__(
        self,
        *,
        target: Name = None,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            target=target,
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Attribute(_auto_ast3_12.Attribute):

    def __init__(
        self,
        *,
        value: expr = None,
        attr: str = "",
        ctx: expr_context = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            attr=attr,
            ctx=ctx,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Slice(_auto_ast3_12.Slice):

    def __init__(
        self,
        *,
        lower: typing.Optional[expr] = None,
        upper: typing.Optional[expr] = None,
        step: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lower=lower,
            upper=upper,
            step=step,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Subscript(_auto_ast3_12.Subscript):

    def __init__(
        self,
        *,
        value: expr = None,
        slice: expr = None,
        ctx: expr_context = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            slice=slice,
            ctx=ctx,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Starred(_auto_ast3_12.Starred):

    def __init__(
        self,
        *,
        value: expr = None,
        ctx: expr_context = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            ctx=ctx,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Name(_auto_ast3_12.Name):

    def __init__(
        self,
        *,
        id: str = "",
        ctx: expr_context = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            id=id,
            ctx=ctx,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class List(_auto_ast3_12.List):

    def __init__(
        self,
        *,
        elts: list[expr] = None,
        ctx: expr_context = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            elts=elts,
            ctx=ctx,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Tuple(_auto_ast3_12.Tuple):

    def __init__(
        self,
        *,
        elts: list[expr] = None,
        ctx: expr_context = None,
        dims: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            elts=elts,
            ctx=ctx,
            dims=dims,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class expr_context(_auto_ast3_12.expr_context):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Del(_auto_ast3_12.Del):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Load(_auto_ast3_12.Load):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Store(_auto_ast3_12.Store):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class boolop(_auto_ast3_12.boolop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class And(_auto_ast3_12.And):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Or(_auto_ast3_12.Or):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class operator(_auto_ast3_12.operator):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Add(_auto_ast3_12.Add):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class BitAnd(_auto_ast3_12.BitAnd):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class BitOr(_auto_ast3_12.BitOr):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class BitXor(_auto_ast3_12.BitXor):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Div(_auto_ast3_12.Div):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class FloorDiv(_auto_ast3_12.FloorDiv):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class LShift(_auto_ast3_12.LShift):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Mod(_auto_ast3_12.Mod):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Mult(_auto_ast3_12.Mult):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class MatMult(_auto_ast3_12.MatMult):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Pow(_auto_ast3_12.Pow):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class RShift(_auto_ast3_12.RShift):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Sub(_auto_ast3_12.Sub):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class unaryop(_auto_ast3_12.unaryop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Invert(_auto_ast3_12.Invert):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Not(_auto_ast3_12.Not):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class UAdd(_auto_ast3_12.UAdd):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class USub(_auto_ast3_12.USub):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class cmpop(_auto_ast3_12.cmpop):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Eq(_auto_ast3_12.Eq):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Gt(_auto_ast3_12.Gt):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class GtE(_auto_ast3_12.GtE):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class In(_auto_ast3_12.In):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Is(_auto_ast3_12.Is):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class IsNot(_auto_ast3_12.IsNot):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Lt(_auto_ast3_12.Lt):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class LtE(_auto_ast3_12.LtE):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class NotEq(_auto_ast3_12.NotEq):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class NotIn(_auto_ast3_12.NotIn):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class comprehension(_auto_ast3_12.comprehension):

    def __init__(
        self,
        *,
        target: expr = None,
        iter: expr = None,
        ifs: list[expr] = None,
        is_async: int = 0,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            target=target,
            iter=iter,
            ifs=ifs,
            is_async=is_async,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class excepthandler(_auto_ast3_12.excepthandler):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class ExceptHandler(_auto_ast3_12.ExceptHandler):

    def __init__(
        self,
        *,
        type: typing.Optional[expr] = None,
        name: typing.Optional[str] = None,
        body: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            type=type,
            name=name,
            body=body,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class arguments(_auto_ast3_12.arguments):

    def __init__(
        self,
        *,
        posonlyargs: list[arg] = None,
        args: list[arg] = None,
        vararg: typing.Optional[arg] = None,
        kwonlyargs: list[arg] = None,
        kw_defaults: list[expr | None] = None,
        kwarg: typing.Optional[arg] = None,
        defaults: list[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            posonlyargs=posonlyargs,
            args=args,
            vararg=vararg,
            kwonlyargs=kwonlyargs,
            kw_defaults=kw_defaults,
            kwarg=kwarg,
            defaults=defaults,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class arg(_auto_ast3_12.arg):

    def __init__(
        self,
        *,
        arg: str = "",
        annotation: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            arg=arg,
            annotation=annotation,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class keyword(_auto_ast3_12.keyword):

    def __init__(
        self,
        *,
        arg: typing.Optional[str] = None,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            arg=arg,
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class alias(_auto_ast3_12.alias):

    def __init__(
        self,
        *,
        name: str = "",
        asname: typing.Optional[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            name=name,
            asname=asname,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class withitem(_auto_ast3_12.withitem):

    def __init__(
        self,
        *,
        context_expr: expr = None,
        optional_vars: typing.Optional[expr] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            context_expr=context_expr,
            optional_vars=optional_vars,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class Match(_auto_ast3_12.Match):

    def __init__(
        self,
        *,
        subject: expr = None,
        cases: list[match_case] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            subject=subject,
            cases=cases,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class pattern(_auto_ast3_12.pattern):

    def __init__(
        self,
        *,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class match_case(_auto_ast3_12.match_case):

    def __init__(
        self,
        *,
        pattern: typing.Pattern = None,
        guard: typing.Optional[expr] = None,
        body: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            pattern=pattern,
            guard=guard,
            body=body,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class MatchValue(_auto_ast3_12.MatchValue):

    def __init__(
        self,
        *,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class MatchSingleton(_auto_ast3_12.MatchSingleton):

    def __init__(
        self,
        *,
        value: typing.Optional[typing.Literal[True, False]] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class MatchSequence(_auto_ast3_12.MatchSequence):

    def __init__(
        self,
        *,
        patterns: list[pattern] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            patterns=patterns,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class MatchStar(_auto_ast3_12.MatchStar):

    def __init__(
        self,
        *,
        name: typing.Optional[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            name=name,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class MatchMapping(_auto_ast3_12.MatchMapping):

    def __init__(
        self,
        *,
        keys: list[expr] = None,
        patterns: list[pattern] = None,
        rest: typing.Optional[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            keys=keys,
            patterns=patterns,
            rest=rest,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class MatchClass(_auto_ast3_12.MatchClass):

    def __init__(
        self,
        *,
        cls: expr = None,
        patterns: list[pattern] = None,
        kwd_attrs: list[str] = None,
        kwd_patterns: list[pattern] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            cls=cls,
            patterns=patterns,
            kwd_attrs=kwd_attrs,
            kwd_patterns=kwd_patterns,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class MatchAs(_auto_ast3_12.MatchAs):

    def __init__(
        self,
        *,
        pattern: typing.Optional[typing.Pattern] = None,
        name: typing.Optional[str] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            pattern=pattern,
            name=name,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class MatchOr(_auto_ast3_12.MatchOr):

    def __init__(
        self,
        *,
        patterns: list[pattern] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            patterns=patterns,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class TryStar(_auto_ast3_12.TryStar):

    def __init__(
        self,
        *,
        body: list[stmt] = None,
        handlers: list[ExceptHandler] = None,
        orelse: list[stmt] = None,
        finalbody: list[stmt] = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            body=body,
            handlers=handlers,
            orelse=orelse,
            finalbody=finalbody,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )


class type_param(_auto_ast3_12.type_param):

    def __init__(
        self,
        *,
        end_lineno: int = 0,
        end_col_offset: int = 0,
        lineno: int = 0,
        col_offset: int = 0,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            lineno=lineno,
            col_offset=col_offset,
            type_comment=type_comment,
        )


class TypeVar(_auto_ast3_12.TypeVar):

    def __init__(
        self,
        *,
        name: str = "",
        bound: typing.Optional[expr] = None,
        end_lineno: int = 0,
        end_col_offset: int = 0,
        lineno: int = 0,
        col_offset: int = 0,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            name=name,
            bound=bound,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            lineno=lineno,
            col_offset=col_offset,
            type_comment=type_comment,
        )


class ParamSpec(_auto_ast3_12.ParamSpec):

    def __init__(
        self,
        *,
        name: str = "",
        end_lineno: int = 0,
        end_col_offset: int = 0,
        lineno: int = 0,
        col_offset: int = 0,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            name=name,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            lineno=lineno,
            col_offset=col_offset,
            type_comment=type_comment,
        )


class TypeVarTuple(_auto_ast3_12.TypeVarTuple):

    def __init__(
        self,
        *,
        name: str = "",
        end_lineno: int = 0,
        end_col_offset: int = 0,
        lineno: int = 0,
        col_offset: int = 0,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            name=name,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            lineno=lineno,
            col_offset=col_offset,
            type_comment=type_comment,
        )


class TypeAlias(_auto_ast3_12.TypeAlias):

    def __init__(
        self,
        *,
        name: Name = None,
        type_params: list[type_param] = None,
        value: expr = None,
        lineno: int = 0,
        col_offset: int = 0,
        end_lineno: typing.Optional[int] = None,
        end_col_offset: typing.Optional[int] = None,
        type_comment: typing.Optional[str] = None,
    ) -> None:
        super().__init__(
            name=name,
            type_params=type_params,
            value=value,
            lineno=lineno,
            col_offset=col_offset,
            end_lineno=end_lineno,
            end_col_offset=end_col_offset,
            type_comment=type_comment,
        )
