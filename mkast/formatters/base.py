from abc import ABC, abstractmethod


class BaseFormattingProvider(ABC):
    ID: str

    def __init__(self) -> None:
        super().__init__()
        assert self.ID is not None
        self.id: str = self.ID

    @abstractmethod
    def formatCode(self, code: str) -> str:
        pass
