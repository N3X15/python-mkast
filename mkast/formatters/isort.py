from typing import Optional

import isort

from mkast.formatters.base import BaseFormattingProvider


class ISortFormattingProvider(BaseFormattingProvider):
    ID: str = "isort"

    def __init__(
        self,
        isort_config: Optional[isort.Config] = None,
    ) -> None:
        super().__init__()
        self.config = isort_config or isort.Config()

    def formatCode(self, code: str) -> str:
        return isort.code(code, config=self.config)
