from typing import Any, Dict

import autoflake

from mkast.formatters.base import BaseFormattingProvider


class AutoflakeFormattingProvider(BaseFormattingProvider):
    ID: str = "autoflake"

    def __init__(
        self,
        additional_imports=None,
        expand_star_imports=False,
        remove_all_unused_imports=False,
        remove_duplicate_keys=False,
        remove_unused_variables=False,
        remove_rhs_for_unused_variables=False,
        ignore_init_module_imports=False,
        ignore_pass_statements=False,
        ignore_pass_after_docstring=False,
    ) -> None:
        super().__init__()
        self.additional_imports = additional_imports
        self.expand_star_imports = expand_star_imports
        self.remove_all_unused_imports = remove_all_unused_imports
        self.remove_duplicate_keys = remove_duplicate_keys
        self.remove_unused_variables = remove_unused_variables
        self.remove_rhs_for_unused_variables = remove_rhs_for_unused_variables
        self.ignore_init_module_imports = ignore_init_module_imports
        self.ignore_pass_statements = ignore_pass_statements
        self.ignore_pass_after_docstring = ignore_pass_after_docstring

    def formatCode(self, code: str) -> str:
        return autoflake.fix_code(
            code,
            additional_imports=self.additional_imports,
            expand_star_imports=self.expand_star_imports,
            remove_all_unused_imports=self.remove_all_unused_imports,
            remove_duplicate_keys=self.remove_duplicate_keys,
            remove_unused_variables=self.remove_unused_variables,
            remove_rhs_for_unused_variables=self.remove_rhs_for_unused_variables,
            ignore_init_module_imports=self.ignore_init_module_imports,
            ignore_pass_statements=self.ignore_pass_statements,
            ignore_pass_after_docstring=self.ignore_pass_after_docstring,
        )
