from typing import Any, Dict

import autopep8

from mkast.formatters.base import BaseFormattingProvider


class Autopep8FormattingProvider(BaseFormattingProvider):
    ID: str = "autopep8"

    def __init__(self, options: Dict[str, Any] = {}) -> None:
        super().__init__()
        self.options = options

    def formatCode(self, code: str) -> str:
        return autopep8.fix_code(code, options=self.options)
