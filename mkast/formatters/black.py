from typing import Optional

import black

from mkast.formatters.base import BaseFormattingProvider


class BlackFormattingProvider(BaseFormattingProvider):
    ID: str = "black"

    def __init__(self, mode: Optional[black.FileMode] = None) -> None:
        super().__init__()
        self.mode = mode or black.FileMode()

    def formatCode(self, code: str) -> str:
        return black.format_str(code, mode=self.mode)
