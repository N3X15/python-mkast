from typing import Dict

from mkast.utils import all_mods_exist

from .base import BaseFormattingProvider

AVAILABLE_FORMATTERS: Dict[str, BaseFormattingProvider] = {}

if all_mods_exist(
    {
        "isort",
    }
):
    from .isort import ISortFormattingProvider

    AVAILABLE_FORMATTERS[ISortFormattingProvider.ID] = ISortFormattingProvider()
if all_mods_exist(
    {
        "autopep8",
    }
):
    from .autopep8 import Autopep8FormattingProvider

    AVAILABLE_FORMATTERS[Autopep8FormattingProvider.ID] = Autopep8FormattingProvider()
if all_mods_exist(
    {
        "black",
    }
):
    from .black import BlackFormattingProvider

    AVAILABLE_FORMATTERS[BlackFormattingProvider.ID] = BlackFormattingProvider()
if all_mods_exist(
    {
        "autoflake",
    }
):
    from .autoflake import AutoflakeFormattingProvider

    AVAILABLE_FORMATTERS[AutoflakeFormattingProvider.ID] = AutoflakeFormattingProvider()
