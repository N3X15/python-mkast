import ast
from typing import Tuple, Type, TypeAlias, Union

from mkast.nodes import ast3_10, ast3_11, ast3_12

LOAD = ast.Load()
STORE = ast.Store()

ExprTypes = set(
    [
        ast.Name,
        ast.Expr,
        ast.Add,
        ast.And,
        ast.Attribute,
        ast.AugAssign,
        ast.BinOp,
        # ast.BitAnd,
        # ast.BitOr,
        # ast.BitXor,
        # ast.BoolOp,
        # ast.Bytes,
        ast.Call,
        ast.Constant,
        ast.Compare,
        ast.Dict,
        # ast.Div,
        # ast.Eq
        ast.UnaryOp,
    ]
)

AnyAST: TypeAlias = Union[ast.AST, ast3_10.AST, ast3_11.AST, ast3_12.AST]
ALL_AST_TYPES: Tuple[Type, ...] = (ast3_10.AST, ast3_11.AST, ast3_12.AST)
