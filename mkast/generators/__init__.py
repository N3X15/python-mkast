from typing import Dict

from mkast.utils import all_mods_exist

from .base import BaseCodeGenerator

AVAILABLE_GENERATORS: Dict[str, BaseCodeGenerator] = {}

if all_mods_exist(
    {
        "astor",
    }
):
    from .astor import AstorCodeGenerator

    AVAILABLE_GENERATORS[AstorCodeGenerator.ID] = AstorCodeGenerator()
