import ast
from abc import ABC, abstractmethod
from typing import List

from mkast.formatters.base import BaseFormattingProvider


class BaseCodeGenerator(ABC):
    ID: str

    def __init__(self) -> None:
        super().__init__()
        assert self.ID is not None
        self.formattingProviders: List[BaseFormattingProvider] = []

    def addFormatter(self, formatter: BaseFormattingProvider) -> None:
        self.formattingProviders.append(formatter)

    @abstractmethod
    def _unparse(self, node: ast.AST) -> str:
        pass

    def generate(self, node: ast.AST) -> str:
        o: str = self._unparse(node)
        for fp in self.formattingProviders:
            o = fp.formatCode(o)
        return o
