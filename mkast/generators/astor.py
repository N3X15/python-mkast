import ast

import astor
from astor.source_repr import split_lines

from mkast.generators.base import BaseCodeGenerator


class AstorCodeGenerator(BaseCodeGenerator):
    ID = "astor"

    def __init__(self) -> None:
        super().__init__()
        self.indent_with: str = " " * 4

    def _unparse(self, node: ast.AST) -> str:
        def pretty_source(source):
            return "".join(split_lines(source, maxline=65535))

        return astor.to_source(
            node, 
            indent_with=self.indent_with, 
            add_line_information=False,
            pretty_source=pretty_source
        )
