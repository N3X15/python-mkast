import ast
import importlib.util
from pathlib import Path
from typing import Any, List, Optional, Set, Type
import typing

from mkast.ast_types import ALL_AST_TYPES, AnyAST


def value2ast(value: Any) -> ast.AST:
    if isinstance(value, (int, float, str, bytes)):
        return ast.Constant(value=value, kind=None)
    if isinstance(value, ast.AST):
        return value
    return None


def all_mods_exist(required_imports: Set[str]) -> bool:
    return all([(importlib.util.find_spec(x) is not None) for x in required_imports])


def type2annostr(T: Optional[Type]) -> str:
    if T is None:
        return "None"
    assert isinstance(T, Type), str(type(T))
    if (O := typing.get_origin(T)) is None:
        return T.__name__
    else:
        A = ", ".join(map(type2annostr, typing.get_args(T)))
        return f"{type2annostr(O)}[{A}]"


def type2astanno(T: Optional[Type]) -> ast.AST:
    if T is None:
        return None
    assert isinstance(T, Type), str(type(T))
    if (O := typing.get_origin(T)) is None:
        return qualname2astname(T.__qualname__)
    else:
        L = list(map(type2astanno, typing.get_args(T)))
        return ast.Subscript(value=type2annostr(O), slice=L)


def qualname2astname(qualname: str, ctx: ast.AST = None) -> ast.AST:
    names = qualname.split(".")
    if len(names) == 1:
        return ast.Name(id=names[0], ctx=ctx)
    prev: ast.AST = ast.Attribute(value=ast.Name(id=names[0], ctx=ctx), attr=names[1])
    if len(names) > 2:
        for i in range(2, len(names)):
            prev = ast.Attribute(value=prev, attr=names[i], ctx=ast.Load())
    return prev


def autobuild(n: Optional[AnyAST]) -> Optional[ast.AST]:
    if n is None:
        return None
    if isinstance(n, ALL_AST_TYPES):
        n = n.build()
    return n


def path2ast(path: Path, prefix: Optional[str] = None) -> ast.AST:
    o: List[ast.AST] = []
    if prefix is not None:
        o.append(ast.Name(id=prefix, ctx=ast.Load()))
    # p:Optional[Path] = None
    for chunk in reversed(path.parents):
        o.append(ast.Constant(chunk.name, kind=None))
    o.append(path.name)
    # from mkast.nodes import ast3_10 as mast
    bop: ast.AST = ast.BinOp(left=o[0], op=ast.Div(), right=o[1])
    for c in o[:2]:
        bop = ast.BinOp(left=bop, op=ast.Div(), right=c)
    return bop
