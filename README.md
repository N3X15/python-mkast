# mkAST

Because I am lazy.

Most other languages have an AST factory system that makes things easier on the poor bastards having to slap together ASTs manually, so, seeing that Python (3, at least) lacks such a thing, I figured I'd be the poor bastard making that factory.

## How2Use

Read the code.

## FEATURE WHEN

When you code it:tm:

## RELEASE WHEN

shrug

## Plans

* [ ] Typed function definitions of Python 3 AST nodes.